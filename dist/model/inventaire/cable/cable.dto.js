"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = require("class-validator");
const connector_dto_1 = require("./connector.dto");
const category_dto_1 = require("./category.dto");
const group_model_1 = require("../../auth/group.model");
const cable_entity_1 = require("../../../entity/cable/cable.entity");
class CableDto {
    constructor(partial) {
        Object.assign(this, partial);
    }
    static fromEntity(entity) {
        const dto = new CableDto();
        dto.id = entity.CAB_Id;
        dto.length = entity.CAB_Length;
        dto.quantity = entity.CAB_Quantity;
        dto.connector_A = connector_dto_1.ConnectorDto.fromEntity(entity.CON_A);
        dto.connector_B = connector_dto_1.ConnectorDto.fromEntity(entity.CON_B);
        dto.category = category_dto_1.CategoryDto.fromEntity(entity.CAT);
        dto.group = group_model_1.GroupDto.fromEntity(entity.GRP);
        return dto;
    }
    toEntity() {
        const entity = new cable_entity_1.Cable();
        if (this.hasOwnProperty('length')) {
            entity.CAB_Length = this.length;
        }
        if (this.hasOwnProperty('quantity')) {
            entity.CAB_Quantity = this.quantity;
        }
        if (this.hasOwnProperty('connector_A')) {
            entity.CON_Id_A = this.connector_A.id;
        }
        if (this.hasOwnProperty('connector_B')) {
            entity.CON_Id_B = this.connector_B.id;
        }
        if (this.hasOwnProperty('category')) {
            entity.CAT_Id = this.category.id;
        }
        if (this.hasOwnProperty('group')) {
            entity.GRP_Id = this.group.id;
        }
        return entity;
    }
}
__decorate([
    class_validator_1.IsUUID(),
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], CableDto.prototype, "id", void 0);
__decorate([
    class_validator_1.IsDefined({ message: "Le champ longueur est obligatoire" }),
    class_validator_1.IsNotEmpty({ message: "Le champ longueur est obligatoire" }),
    class_validator_1.IsPositive({ message: "La longueur doit être spécifiée et supérieur à 0" }),
    class_validator_1.Min(0.1, { message: "La longueur ne peut pas faire moins de 0.1m" }),
    class_validator_1.Max(1000000, { message: "Vous faites des liasons transatlantique au SIEN maintenant ?" }),
    __metadata("design:type", Number)
], CableDto.prototype, "length", void 0);
__decorate([
    class_validator_1.IsDefined({ message: "Le champ quantité est obligatoire" }),
    class_validator_1.IsPositive({ message: "La longueur doit être spécifiée et supérieur à 0" }),
    class_validator_1.Min(1, { message: "La quantité doit au moins compter un élément" }),
    class_validator_1.Max(1000, { message: "La quantité ne peut pas excéder 1000 pièces par requête" }),
    __metadata("design:type", Number)
], CableDto.prototype, "quantity", void 0);
__decorate([
    class_validator_1.IsNotEmptyObject({ message: "Il est nécessaire de spécifier les deux connecteurs" }),
    __metadata("design:type", connector_dto_1.ConnectorDto)
], CableDto.prototype, "connector_A", void 0);
__decorate([
    class_validator_1.IsNotEmptyObject({ message: "Il est nécessaire de spécifier les deux connecteurs" }),
    __metadata("design:type", connector_dto_1.ConnectorDto)
], CableDto.prototype, "connector_B", void 0);
__decorate([
    class_validator_1.IsNotEmptyObject({ message: "Impossible d'insérer un câble sans catégorie" }),
    __metadata("design:type", category_dto_1.CategoryDto)
], CableDto.prototype, "category", void 0);
__decorate([
    class_validator_1.IsNotEmptyObject({ message: "Un câble doit obligatoirement faire partie groupe" }),
    __metadata("design:type", group_model_1.GroupDto)
], CableDto.prototype, "group", void 0);
exports.CableDto = CableDto;
//# sourceMappingURL=cable.dto.js.map
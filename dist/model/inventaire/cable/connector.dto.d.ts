import { Connector } from '../../../entity/cable/connector.entity';
export declare class ConnectorDto {
    id: string;
    name: string;
    constructor(partial?: Partial<ConnectorDto>);
    static fromEntity(entity: Connector): ConnectorDto;
    toEntity(): Connector;
}

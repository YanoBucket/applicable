"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = require("class-validator");
const category_entity_1 = require("../../../entity/cable/category.entity");
class CategoryDto {
    constructor(partial) {
        Object.assign(this, partial);
    }
    static fromEntity(entity) {
        const dto = new CategoryDto();
        dto.id = entity.CAB_CAT_Id;
        dto.name = entity.CAB_CAT_Name;
        return dto;
    }
    toEntity() {
        const entity = new category_entity_1.Category();
        if (this.hasOwnProperty('id')) {
            entity.CAB_CAT_Id = this.id;
        }
        if (this.hasOwnProperty('name')) {
            entity.CAB_CAT_Name = this.name;
        }
        return entity;
    }
}
__decorate([
    class_validator_1.IsUUID(),
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], CategoryDto.prototype, "id", void 0);
__decorate([
    class_validator_1.IsDefined({ message: "Impossible d'insérer une catégorie sans nom" }),
    class_validator_1.IsString({ message: "Le nom de la catégorie doit être une chaîne de caratères valide" }),
    class_validator_1.MaxLength(25, { message: "La catégorie ne peut par faire plus de 25 caractères" }),
    __metadata("design:type", String)
], CategoryDto.prototype, "name", void 0);
exports.CategoryDto = CategoryDto;
//# sourceMappingURL=category.dto.js.map
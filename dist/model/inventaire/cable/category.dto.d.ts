import { Category } from '../../../entity/cable/category.entity';
export declare class CategoryDto {
    id: string;
    name: string;
    constructor(partial?: Partial<CategoryDto>);
    static fromEntity(entity: Category): CategoryDto;
    toEntity(): Category;
}

import { ConnectorDto } from './connector.dto';
import { CategoryDto } from './category.dto';
import { GroupDto } from '../../auth/group.model';
import { Cable } from '../../../entity/cable/cable.entity';
export declare class CableDto {
    id: string;
    length: number;
    quantity: number;
    connector_A: ConnectorDto;
    connector_B: ConnectorDto;
    category: CategoryDto;
    group: GroupDto;
    constructor(partial?: Partial<CableDto>);
    static fromEntity(entity: Cable): CableDto;
    toEntity(): Cable;
}

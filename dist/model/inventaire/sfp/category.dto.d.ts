import { Category } from '../../../entity/sfp/category.entity';
export declare class CategoryDto {
    id: string;
    name: string;
    constructor(partial?: Partial<CategoryDto>);
    static fromEntity(entity: Category): CategoryDto;
    toEntity(): Category;
}

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = require("class-validator");
const group_model_1 = require("../../auth/group.model");
const category_dto_1 = require("../sfp/category.dto");
const sfp_entity_1 = require("../../../entity/sfp/sfp.entity");
class SfpDto {
    constructor(partial = null) {
        Object.assign(this, partial);
    }
    static fromEntity(entity) {
        const dto = new SfpDto();
        dto.id = entity.SFP_Id;
        dto.speed = entity.SFP_speed;
        dto.quantity = entity.SFP_quantity;
        dto.utilisation = entity.SFP_Utilisation;
        dto.comment = entity.SFP_Comment;
        dto.category = category_dto_1.CategoryDto.fromEntity(entity.CAT);
        dto.group = group_model_1.GroupDto.fromEntity(entity.GRP);
        return dto;
    }
    toEntity() {
        const entity = new sfp_entity_1.Sfp();
        if (this.hasOwnProperty('speed')) {
            entity.SFP_speed = this.speed;
        }
        if (this.hasOwnProperty('quantity')) {
            entity.SFP_quantity = this.quantity;
        }
        if (this.hasOwnProperty('utilisation')) {
            entity.SFP_Utilisation = this.utilisation;
        }
        if (this.hasOwnProperty('comment')) {
            entity.SFP_Comment = this.comment;
        }
        if (this.hasOwnProperty('category')) {
            entity.CAT_Id = this.category.id;
        }
        if (this.hasOwnProperty('group')) {
            entity.GRP_Id = this.group.id;
        }
        return entity;
    }
}
__decorate([
    class_validator_1.IsUUID(),
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], SfpDto.prototype, "id", void 0);
__decorate([
    class_validator_1.IsDefined({ message: "Le champ vitesse est obligatoire" }),
    class_validator_1.IsPositive({ message: "La vitesse doit être spécifiée et supérieur à 0" }),
    class_validator_1.Min(0.1, { message: "La vitesse doit faire au moins 100 Mbps" }),
    class_validator_1.Max(10000, { message: "La vitesse ne peut pas excéder 10'000 Gbps" }),
    __metadata("design:type", Number)
], SfpDto.prototype, "speed", void 0);
__decorate([
    class_validator_1.IsDefined({ message: "Le champ quantité est obligatoire" }),
    class_validator_1.IsPositive({ message: "La quantité doit être spécifiée et supérieur à 0" }),
    class_validator_1.Min(1, { message: "La quantité doit au moins compter un élément" }),
    class_validator_1.Max(1000, { message: "La quantité ne peut pas excéder 1000 pièces par requête" }),
    __metadata("design:type", Number)
], SfpDto.prototype, "quantity", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsOptional(),
    class_validator_1.MaxLength(50, { message: "Le champ utilisation ne peut pas dépasser 50 caractères" }),
    __metadata("design:type", String)
], SfpDto.prototype, "utilisation", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsOptional(),
    class_validator_1.MaxLength(100, { message: "Le champ commentaire ne peut pas dépasser 100 caractères" }),
    __metadata("design:type", String)
], SfpDto.prototype, "comment", void 0);
__decorate([
    class_validator_1.IsNotEmptyObject({ message: "Impossible d'insérer un câble sans catégorie" }),
    __metadata("design:type", category_dto_1.CategoryDto)
], SfpDto.prototype, "category", void 0);
__decorate([
    class_validator_1.IsNotEmptyObject({ message: "Un SFP doit obligatoirement faire partie groupe" }),
    __metadata("design:type", group_model_1.GroupDto)
], SfpDto.prototype, "group", void 0);
exports.SfpDto = SfpDto;
//# sourceMappingURL=sfp.dto.js.map
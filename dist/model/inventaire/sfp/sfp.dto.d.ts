import { GroupDto } from '../../auth/group.model';
import { CategoryDto } from '../sfp/category.dto';
import { Sfp } from 'src/entity/sfp/sfp.entity';
export declare class SfpDto {
    id: string;
    speed: number;
    quantity: number;
    utilisation: string;
    comment: string;
    category: CategoryDto;
    group: GroupDto;
    constructor(partial?: Partial<SfpDto>);
    static fromEntity(entity: Sfp): SfpDto;
    toEntity(): Sfp;
}

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = require("class-validator");
const group_entity_1 = require("../../entity/auth/group.entity");
const type_model_1 = require("./type.model");
class GroupDto {
    constructor(partial) {
        Object.assign(this, partial);
    }
    static fromEntity(entity) {
        const dto = new GroupDto();
        dto.id = entity.GRP_Id;
        dto.name = entity.GRP_Name;
        if (entity.TYP) {
            dto.type = type_model_1.TypeDto.fromEntity(entity.TYP);
        }
        return dto;
    }
    toEntity() {
        const entity = new group_entity_1.Group();
        if (this.hasOwnProperty('id')) {
            entity.GRP_Id = this.id;
        }
        if (this.hasOwnProperty('name')) {
            entity.GRP_Name = this.name;
        }
        if (this.hasOwnProperty('type')) {
            entity.TYP_Id = this.type.id;
        }
        return entity;
    }
}
__decorate([
    class_validator_1.IsUUID(),
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], GroupDto.prototype, "id", void 0);
__decorate([
    class_validator_1.IsDefined({ message: "Le nom du groupe doit être spécifié" }),
    class_validator_1.IsString({ message: "Le nom du groupe doit être une chaîne de caratères valide" }),
    class_validator_1.MinLength(3, { message: "Le nom du groupe ne peut par faire moins de 3 caractères" }),
    class_validator_1.MaxLength(15, { message: "Le nom du groupe ne peut par faire plus de 15 caractères" }),
    __metadata("design:type", String)
], GroupDto.prototype, "name", void 0);
__decorate([
    class_validator_1.IsNotEmptyObject({ message: "Il est nécessaire de spécifier un type de groupe" }),
    __metadata("design:type", type_model_1.TypeDto)
], GroupDto.prototype, "type", void 0);
exports.GroupDto = GroupDto;
//# sourceMappingURL=group.model.js.map
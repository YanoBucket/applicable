"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = require("class-validator");
const group_model_1 = require("./group.model");
const role_model_1 = require("./role.model");
const user_entity_1 = require("../../entity/auth/user.entity");
class UserDto {
    constructor(partial) {
        Object.assign(this, partial);
    }
    static fromEntity(entity) {
        const dto = new UserDto();
        dto.id = entity.USE_Id;
        dto.username = entity.USE_Username;
        if (entity.USE_Role) {
            dto.role = role_model_1.RoleDto.fromEntity(entity.USE_Role);
        }
        if (entity.USE_Groups) {
            dto.groups = [];
            entity.USE_Groups.forEach(group => {
                dto.groups.push(group_model_1.GroupDto.fromEntity(group));
            });
        }
        return dto;
    }
    toEntity() {
        const entity = new user_entity_1.User();
        if (this.hasOwnProperty('id')) {
            entity.USE_Id = this.id;
        }
        if (this.hasOwnProperty('password')) {
            entity.USE_Password = this.password;
        }
        if (this.hasOwnProperty('username')) {
            entity.USE_Username = this.username;
        }
        if (this.hasOwnProperty('role')) {
            entity.USE_Role = new role_model_1.RoleDto(this.role).toEntity();
        }
        if (this.hasOwnProperty('groups')) {
            entity.USE_Groups = [];
            this.groups.forEach(group => {
                entity.USE_Groups.push(new group_model_1.GroupDto(group).toEntity());
            });
        }
        return entity;
    }
}
__decorate([
    class_validator_1.IsUUID(),
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], UserDto.prototype, "id", void 0);
__decorate([
    class_validator_1.IsDefined({ message: "Le nom d'utilisateur doit être spécifié" }),
    class_validator_1.IsString({ message: "Le nom d'utilisateur doit être une chaîne de caratères valide" }),
    class_validator_1.MinLength(3, { message: "Le nom d'utilisateur ne peut par faire moins de 3 caractères" }),
    class_validator_1.MaxLength(15, { message: "Le nom d'utilisateur ne peut par faire plus de 15 caractères" }),
    __metadata("design:type", String)
], UserDto.prototype, "username", void 0);
__decorate([
    class_validator_1.IsNotEmptyObject({ message: "Il est nécessaire de spécifier un rôle" }),
    __metadata("design:type", role_model_1.RoleDto)
], UserDto.prototype, "role", void 0);
__decorate([
    class_validator_1.IsDefined({ message: "Le mot de passe doit être spécifié" }),
    class_validator_1.IsString({ message: "Le mot de passe doit être une chaîne de caratères valide" }),
    class_validator_1.MinLength(8, { message: "Le mot de passe doit faire minimum 8 caractères" }),
    class_validator_1.MaxLength(20, { message: "Le mot de passe ne peut pas dépasser 20 caractères" }),
    __metadata("design:type", String)
], UserDto.prototype, "password", void 0);
exports.UserDto = UserDto;
//# sourceMappingURL=user.model.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const role_entity_1 = require("../../entity/auth/role.entity");
class RoleDto {
    constructor(partial) {
        Object.assign(this, partial);
    }
    static fromEntity(entity) {
        const dto = new RoleDto();
        dto.id = entity.ROL_Id;
        dto.name = entity.ROL_Name;
        return dto;
    }
    toEntity() {
        const entity = new role_entity_1.Role();
        if (this.hasOwnProperty('id')) {
            entity.ROL_Id = this.id;
        }
        if (this.hasOwnProperty('name')) {
            entity.ROL_Name = this.name;
        }
        return entity;
    }
}
exports.RoleDto = RoleDto;
//# sourceMappingURL=role.model.js.map
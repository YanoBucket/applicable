import { Type } from 'src/entity/auth/type.entity';
export declare class TypeDto {
    id: string;
    name: string;
    constructor(partial?: Partial<TypeDto>);
    static fromEntity(entity: Type): TypeDto;
    toEntity(): Type;
}

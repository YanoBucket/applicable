import { Role } from "src/entity/auth/role.entity";
export declare class RoleDto {
    id: string;
    name: string;
    constructor(partial?: Partial<RoleDto>);
    static fromEntity(entity: Role): RoleDto;
    toEntity(): Role;
}

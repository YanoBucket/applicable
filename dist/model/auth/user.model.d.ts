import { GroupDto } from "./group.model";
import { RoleDto } from "./role.model";
import { User } from "src/entity/auth/user.entity";
export declare class UserDto {
    id: string;
    username: string;
    role: RoleDto;
    groups: GroupDto[];
    password: string;
    constructor(partial?: Partial<UserDto>);
    static fromEntity(entity: User): UserDto;
    toEntity(): User;
}

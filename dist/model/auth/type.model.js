"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const type_entity_1 = require("../../entity/auth/type.entity");
class TypeDto {
    constructor(partial) {
        Object.assign(this, partial);
    }
    static fromEntity(entity) {
        const dto = new TypeDto();
        dto.id = entity.GRP_TYP_Id;
        dto.name = entity.GRP_TYP_Name;
        return dto;
    }
    toEntity() {
        const entity = new type_entity_1.Type();
        if (this.hasOwnProperty('id')) {
            entity.GRP_TYP_Id = this.id;
        }
        if (this.hasOwnProperty('name')) {
            entity.GRP_TYP_Name = this.name;
        }
        return entity;
    }
}
exports.TypeDto = TypeDto;
//# sourceMappingURL=type.model.js.map
import { Group } from "src/entity/auth/group.entity";
import { TypeDto } from "./type.model";
export declare class GroupDto {
    id: string;
    name: string;
    type: TypeDto;
    constructor(partial?: Partial<GroupDto>);
    static fromEntity(entity: Group): GroupDto;
    toEntity(): Group;
}

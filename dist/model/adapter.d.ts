import { Logger } from "@nestjs/common";
export declare class Adapter {
    logger: Logger;
    fromEntity<DTO, ENTITY>(dto: DTO, entity: ENTITY): void;
}

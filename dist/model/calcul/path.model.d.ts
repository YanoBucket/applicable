import { Path } from 'src/entity/calcul/path.entity';
import { GroupDto } from '../auth/group.model';
export declare class PathDto {
    id: string;
    compartment_A: string;
    compartment_B: string;
    distance: number;
    group: GroupDto;
    constructor(partial?: Partial<PathDto>);
    static fromEntity(entity: Path): PathDto;
    toEntity(): Path;
}

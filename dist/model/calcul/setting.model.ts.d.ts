import { GroupDto } from '../auth/group.model';
import { Setting } from 'src/entity/calcul/setting.entity';
export declare class SettingDto {
    id: string;
    key: string;
    value: number;
    group: GroupDto;
    constructor(partial?: Partial<SettingDto>);
    static fromEntity(entity: Setting): SettingDto;
    toEntity(): Setting;
}

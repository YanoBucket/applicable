"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = require("class-validator");
const group_model_1 = require("../auth/group.model");
const setting_entity_1 = require("../../entity/calcul/setting.entity");
class SettingDto {
    constructor(partial) {
        Object.assign(this, partial);
    }
    static fromEntity(entity) {
        const dto = new SettingDto();
        dto.id = entity.SET_Id;
        dto.key = entity.SET_Key;
        dto.value = entity.SET_Value;
        dto.group = group_model_1.GroupDto.fromEntity(entity.GRP);
        return dto;
    }
    toEntity() {
        const entity = new setting_entity_1.Setting();
        if (this.hasOwnProperty('id')) {
            entity.SET_Id = this.id;
        }
        if (this.hasOwnProperty('key')) {
            entity.SET_Key = this.key;
        }
        if (this.hasOwnProperty('value')) {
            entity.SET_Value = this.value;
        }
        if (this.hasOwnProperty('group')) {
            entity.GRP_Id = this.group.id;
        }
        return entity;
    }
}
__decorate([
    class_validator_1.IsUUID(),
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], SettingDto.prototype, "id", void 0);
__decorate([
    class_validator_1.IsDefined({ message: "Impossible d'insérer une clé vide" }),
    class_validator_1.IsString({ message: "La clé doit être une chaîne de caratères valide" }),
    class_validator_1.MaxLength(20, { message: "La clé ne peut par faire plus de 20 caractères" }),
    __metadata("design:type", String)
], SettingDto.prototype, "key", void 0);
__decorate([
    class_validator_1.IsDefined({ message: "Impossible d'insérer une valeur vide" }),
    class_validator_1.IsNotEmpty({ message: "Impossible d'insérer une valeur vide" }),
    class_validator_1.Min(0, { message: "Impossible d'insérer une valeur inférieur à 0" }),
    __metadata("design:type", Number)
], SettingDto.prototype, "value", void 0);
__decorate([
    class_validator_1.IsNotEmptyObject({
        message: "Un paramètre doit obligatoirement faire partie groupe"
    }),
    __metadata("design:type", group_model_1.GroupDto)
], SettingDto.prototype, "group", void 0);
exports.SettingDto = SettingDto;
//# sourceMappingURL=setting.model.ts.js.map
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = require("class-validator");
const path_entity_1 = require("../../entity/calcul/path.entity");
const group_model_1 = require("../auth/group.model");
class PathDto {
    constructor(partial) {
        Object.assign(this, partial);
    }
    static fromEntity(entity) {
        const dto = new PathDto();
        dto.id = entity.PAT_Id;
        dto.distance = entity.PAT_Distance;
        dto.compartment_A = entity.PAT_Compartment_A;
        dto.compartment_B = entity.PAT_Compartment_B;
        dto.group = group_model_1.GroupDto.fromEntity(entity.GRP);
        return dto;
    }
    toEntity() {
        const entity = new path_entity_1.Path();
        if (this.hasOwnProperty('id')) {
            entity.PAT_Id = this.id;
        }
        if (this.hasOwnProperty('distance')) {
            entity.PAT_Distance = this.distance;
        }
        if (this.hasOwnProperty('compartment_A')) {
            entity.PAT_Compartment_A = this.compartment_A;
        }
        if (this.hasOwnProperty('compartment_B')) {
            entity.PAT_Compartment_B = this.compartment_B;
        }
        if (this.hasOwnProperty('group')) {
            entity.GRP_Id = this.group.id;
        }
        return entity;
    }
}
__decorate([
    class_validator_1.IsUUID(),
    class_validator_1.IsOptional(),
    __metadata("design:type", String)
], PathDto.prototype, "id", void 0);
__decorate([
    class_validator_1.IsDefined({ message: "Les deux compartiments doivent être valués" }),
    class_validator_1.IsString({ message: "Le compartiment A doit être une chaîne de caratères valide" }),
    class_validator_1.MaxLength(8, { message: "Le compartiment A ne peut par faire plus de 8 caractères" }),
    __metadata("design:type", String)
], PathDto.prototype, "compartment_A", void 0);
__decorate([
    class_validator_1.IsDefined({ message: "Les deux compartiments doivent être valués" }),
    class_validator_1.IsString({ message: "Le compartiment B doit être une chaîne de caratères valide" }),
    class_validator_1.MaxLength(8, { message: "Le compartiment B ne peut par faire plus de 8 caractères" }),
    __metadata("design:type", String)
], PathDto.prototype, "compartment_B", void 0);
__decorate([
    class_validator_1.IsDefined({ message: "Impossible d'insérer un chemin sans distance" }),
    class_validator_1.IsNotEmpty({ message: "Impossible d'insérer un chemin sans distance" }),
    class_validator_1.Min(0, { message: "Impossible d'insérer un chemin avec une distance inférieur à 0" }),
    __metadata("design:type", Number)
], PathDto.prototype, "distance", void 0);
__decorate([
    class_validator_1.IsNotEmptyObject({
        message: "Un chemin doit obligatoirement faire partie d'un groupe"
    }),
    __metadata("design:type", group_model_1.GroupDto)
], PathDto.prototype, "group", void 0);
exports.PathDto = PathDto;
//# sourceMappingURL=path.model.js.map
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const app_controller_1 = require("./app.controller");
const app_service_1 = require("./app.service");
const core_1 = require("@nestjs/core");
const http_error_filter_1 = require("./common/filters/http-error.filter");
const logging_interceptor_1 = require("./common/interceptors/logging.interceptor");
const typeorm_1 = require("@nestjs/typeorm");
const sfp_module_1 = require("./module/inventaire/sfp/sfp.module");
const cable_module_1 = require("./module/inventaire/cable/cable.module");
const auth_module_1 = require("./module/auth/auth.module");
const group_module_1 = require("./module/group/group.module");
const user_module_1 = require("./module/user/user.module");
const serve_static_1 = require("@nestjs/serve-static");
const path_1 = require("path");
const path_module_1 = require("./module/calcul/path/path.module");
const setting_module_1 = require("./module/calcul/setting/setting.module");
let AppModule = class AppModule {
};
AppModule = __decorate([
    common_1.Module({
        imports: [
            typeorm_1.TypeOrmModule.forRoot(),
            serve_static_1.ServeStaticModule.forRoot({
                rootPath: path_1.join(__dirname, 'client'),
            }),
            sfp_module_1.SfpModule,
            cable_module_1.CableModule,
            auth_module_1.AuthModule,
            user_module_1.UserModule,
            group_module_1.GroupModule,
            path_module_1.PathModule,
            setting_module_1.SettingModule
        ],
        controllers: [app_controller_1.AppController],
        providers: [
            app_service_1.AppService,
            {
                provide: core_1.APP_FILTER,
                useClass: http_error_filter_1.HttpErrorFilter
            },
            {
                provide: core_1.APP_INTERCEPTOR,
                useClass: logging_interceptor_1.LoggingInterceptor
            },
        ],
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map
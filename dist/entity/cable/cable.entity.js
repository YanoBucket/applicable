"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const connector_entity_1 = require("./connector.entity");
const category_entity_1 = require("./category.entity");
const group_entity_1 = require("../auth/group.entity");
let Cable = class Cable {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], Cable.prototype, "CAB_Id", void 0);
__decorate([
    typeorm_1.Column("double"),
    __metadata("design:type", Number)
], Cable.prototype, "CAB_Length", void 0);
__decorate([
    typeorm_1.Column("int"),
    __metadata("design:type", Number)
], Cable.prototype, "CAB_Quantity", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Cable.prototype, "CON_Id_A", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Cable.prototype, "CON_Id_B", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Cable.prototype, "CAT_Id", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Cable.prototype, "GRP_Id", void 0);
__decorate([
    typeorm_1.ManyToOne(type => connector_entity_1.Connector, connector => connector.CON_Id_A),
    typeorm_1.JoinColumn({ name: "CON_Id_A" }),
    __metadata("design:type", connector_entity_1.Connector)
], Cable.prototype, "CON_A", void 0);
__decorate([
    typeorm_1.ManyToOne(type => connector_entity_1.Connector, connector => connector.CON_Id_B),
    typeorm_1.JoinColumn({ name: "CON_Id_B" }),
    __metadata("design:type", connector_entity_1.Connector)
], Cable.prototype, "CON_B", void 0);
__decorate([
    typeorm_1.ManyToOne(type => category_entity_1.Category, category => category.CAB_CAT_Id),
    typeorm_1.JoinColumn({ name: "CAT_Id" }),
    __metadata("design:type", category_entity_1.Category)
], Cable.prototype, "CAT", void 0);
__decorate([
    typeorm_1.ManyToOne(type => group_entity_1.Group, group => group.GRP_Id),
    typeorm_1.JoinColumn({ name: "GRP_Id" }),
    __metadata("design:type", group_entity_1.Group)
], Cable.prototype, "GRP", void 0);
Cable = __decorate([
    typeorm_1.Entity('t_cable')
], Cable);
exports.Cable = Cable;
//# sourceMappingURL=cable.entity.js.map
import { Cable } from './cable.entity';
export declare class Connector {
    CON_Id: string;
    CON_Name: string;
    CON_Id_A: Cable[];
    CON_Id_B: Cable[];
}

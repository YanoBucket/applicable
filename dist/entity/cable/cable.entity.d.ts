import { Connector } from './connector.entity';
import { Category } from './category.entity';
import { Group } from '../auth/group.entity';
export declare class Cable {
    CAB_Id: string;
    CAB_Length: number;
    CAB_Quantity: number;
    CON_Id_A: string;
    CON_Id_B: string;
    CAT_Id: string;
    GRP_Id: string;
    CON_A: Connector;
    CON_B: Connector;
    CAT: Category;
    GRP: Group;
}

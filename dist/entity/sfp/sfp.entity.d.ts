import { Category } from './category.entity';
import { Group } from '../auth/group.entity';
export declare class Sfp {
    SFP_Id: string;
    SFP_speed: number;
    SFP_quantity: number;
    SFP_Utilisation: string;
    SFP_Comment: string;
    CAT_Id: string;
    GRP_Id: string;
    CAT: Category;
    GRP: Group;
}

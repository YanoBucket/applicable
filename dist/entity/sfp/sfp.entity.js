"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const category_entity_1 = require("./category.entity");
const group_entity_1 = require("../auth/group.entity");
let Sfp = class Sfp {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], Sfp.prototype, "SFP_Id", void 0);
__decorate([
    typeorm_1.Column({ type: "int", }),
    __metadata("design:type", Number)
], Sfp.prototype, "SFP_speed", void 0);
__decorate([
    typeorm_1.Column({ type: "int" }),
    __metadata("design:type", Number)
], Sfp.prototype, "SFP_quantity", void 0);
__decorate([
    typeorm_1.Column({
        type: "varchar",
        length: 50,
        default: null
    }),
    __metadata("design:type", String)
], Sfp.prototype, "SFP_Utilisation", void 0);
__decorate([
    typeorm_1.Column({
        type: "varchar",
        length: 100,
        default: null
    }),
    __metadata("design:type", String)
], Sfp.prototype, "SFP_Comment", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Sfp.prototype, "CAT_Id", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Sfp.prototype, "GRP_Id", void 0);
__decorate([
    typeorm_1.ManyToOne(type => category_entity_1.Category, category => category.SFP_CAT_Id),
    typeorm_1.JoinColumn({ name: "CAT_Id" }),
    __metadata("design:type", category_entity_1.Category)
], Sfp.prototype, "CAT", void 0);
__decorate([
    typeorm_1.ManyToOne(type => group_entity_1.Group, group => group.GRP_Id),
    typeorm_1.JoinColumn({ name: "GRP_Id" }),
    __metadata("design:type", group_entity_1.Group)
], Sfp.prototype, "GRP", void 0);
Sfp = __decorate([
    typeorm_1.Entity('t_sfp')
], Sfp);
exports.Sfp = Sfp;
//# sourceMappingURL=sfp.entity.js.map
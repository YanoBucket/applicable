import { Sfp } from './sfp.entity';
export declare class Category {
    SFP_CAT_Id: string;
    SFP_CAT_Name: string;
    SFP_Id: Sfp[];
}

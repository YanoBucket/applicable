"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const group_entity_1 = require("../auth/group.entity");
let Setting = class Setting {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], Setting.prototype, "SET_Id", void 0);
__decorate([
    typeorm_1.Column("varchar", { length: 20 }),
    __metadata("design:type", String)
], Setting.prototype, "SET_Key", void 0);
__decorate([
    typeorm_1.Column("float"),
    __metadata("design:type", Number)
], Setting.prototype, "SET_Value", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Setting.prototype, "GRP_Id", void 0);
__decorate([
    typeorm_1.ManyToOne(type => group_entity_1.Group, group => group.GRP_Id),
    typeorm_1.JoinColumn({ name: "GRP_Id" }),
    __metadata("design:type", group_entity_1.Group)
], Setting.prototype, "GRP", void 0);
Setting = __decorate([
    typeorm_1.Entity('t_setting'),
    typeorm_1.Index("IDX_Key", ["SET_Key", "GRP_Id",], { unique: true })
], Setting);
exports.Setting = Setting;
//# sourceMappingURL=setting.entity.js.map
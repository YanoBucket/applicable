"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const group_entity_1 = require("../auth/group.entity");
let Path = class Path {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], Path.prototype, "PAT_Id", void 0);
__decorate([
    typeorm_1.Column("double"),
    __metadata("design:type", Number)
], Path.prototype, "PAT_Distance", void 0);
__decorate([
    typeorm_1.Column("char", { length: 8 }),
    __metadata("design:type", Object)
], Path.prototype, "PAT_Compartment_A", void 0);
__decorate([
    typeorm_1.Column("char", { length: 8 }),
    __metadata("design:type", Object)
], Path.prototype, "PAT_Compartment_B", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Path.prototype, "GRP_Id", void 0);
__decorate([
    typeorm_1.ManyToOne(type => group_entity_1.Group, group => group.GRP_Id),
    typeorm_1.JoinColumn({ name: "GRP_Id" }),
    __metadata("design:type", group_entity_1.Group)
], Path.prototype, "GRP", void 0);
Path = __decorate([
    typeorm_1.Entity('t_path'),
    typeorm_1.Index("IDX_Comp", ["PAT_Compartment_A", "PAT_Compartment_B", "GRP_Id"], { unique: true })
], Path);
exports.Path = Path;
//# sourceMappingURL=path.entity.js.map
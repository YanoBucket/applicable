import { Group } from '../auth/group.entity';
export declare class Path {
    PAT_Id: string;
    PAT_Distance: number;
    PAT_Compartment_A: any;
    PAT_Compartment_B: any;
    GRP_Id: string;
    GRP: Group;
}

import { Group } from '../auth/group.entity';
export declare class Setting {
    SET_Id: string;
    SET_Key: string;
    SET_Value: number;
    GRP_Id: string;
    GRP: Group;
}

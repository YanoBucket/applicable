import { Cable } from '../cable/cable.entity';
import { Type } from './type.entity';
import { User } from './user.entity';
export declare class Group {
    GRP_Id: string;
    GRP_Name: string;
    TYP_Id: string;
    GRP_CAB_Id: Cable[];
    GRP_SFP_Id: Cable[];
    TYP: Type;
    users: User[];
}

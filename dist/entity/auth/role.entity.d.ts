import { User } from './user.entity';
export declare class Role {
    ROL_Id: string;
    ROL_Name: string;
    ROL_Users: User[];
}

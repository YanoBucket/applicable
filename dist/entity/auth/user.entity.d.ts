import { Group } from './group.entity';
import { Role } from './role.entity';
export declare class User {
    USE_Id: string;
    USE_Username: string;
    USE_Password: string;
    USE_Groups: Group[];
    USE_Role: Role;
    hashPassword(): Promise<void>;
    toLowerCase(): Promise<void>;
    hashUpdatedPassword(): Promise<void>;
    comparePassword(attempt: string): Promise<any>;
    toJSON(): Object;
}

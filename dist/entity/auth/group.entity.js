"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const cable_entity_1 = require("../cable/cable.entity");
const sfp_entity_1 = require("../sfp/sfp.entity");
const type_entity_1 = require("./type.entity");
const user_entity_1 = require("./user.entity");
let Group = class Group {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], Group.prototype, "GRP_Id", void 0);
__decorate([
    typeorm_1.Column({
        type: "varchar",
        length: "25"
    }),
    __metadata("design:type", String)
], Group.prototype, "GRP_Name", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Group.prototype, "TYP_Id", void 0);
__decorate([
    typeorm_1.OneToMany(type => cable_entity_1.Cable, cable => cable.GRP_Id),
    __metadata("design:type", Array)
], Group.prototype, "GRP_CAB_Id", void 0);
__decorate([
    typeorm_1.OneToMany(type => sfp_entity_1.Sfp, sfp => sfp.GRP_Id),
    __metadata("design:type", Array)
], Group.prototype, "GRP_SFP_Id", void 0);
__decorate([
    typeorm_1.ManyToOne(type => type_entity_1.Type, type => type.GRP_TYP_Id),
    typeorm_1.JoinColumn({ name: "TYP_Id" }),
    __metadata("design:type", type_entity_1.Type)
], Group.prototype, "TYP", void 0);
__decorate([
    typeorm_1.ManyToMany(type => user_entity_1.User, user => user.USE_Groups),
    __metadata("design:type", Array)
], Group.prototype, "users", void 0);
Group = __decorate([
    typeorm_1.Entity('t_group'),
    typeorm_1.Index(["GRP_Name", "TYP_Id"], { unique: true })
], Group);
exports.Group = Group;
//# sourceMappingURL=group.entity.js.map
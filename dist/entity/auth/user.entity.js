"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const class_transformer_1 = require("class-transformer");
const bcrypt = require("bcryptjs");
const group_entity_1 = require("./group.entity");
const role_entity_1 = require("./role.entity");
let User = class User {
    async hashPassword() {
        this.USE_Password = await bcrypt.hash(this.USE_Password, 10);
    }
    async toLowerCase() {
        this.USE_Username = this.USE_Username.toLowerCase();
    }
    async hashUpdatedPassword() {
        this.USE_Password = await bcrypt.hash(this.USE_Password, 10);
    }
    async comparePassword(attempt) {
        return await bcrypt.compare(attempt, this.USE_Password);
    }
    toJSON() {
        return class_transformer_1.classToPlain(this);
    }
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], User.prototype, "USE_Id", void 0);
__decorate([
    typeorm_1.Column({
        type: "varchar",
        length: 20,
        unique: true
    }),
    __metadata("design:type", String)
], User.prototype, "USE_Username", void 0);
__decorate([
    class_transformer_1.Exclude(),
    typeorm_1.Column(),
    __metadata("design:type", String)
], User.prototype, "USE_Password", void 0);
__decorate([
    typeorm_1.ManyToMany(type => group_entity_1.Group, group => group.users, {
        cascade: true,
        onDelete: "CASCADE",
        onUpdate: "CASCADE"
    }),
    typeorm_1.JoinTable({
        name: 't_user_groups',
        joinColumns: [{ name: 'USE_Id' }],
        inverseJoinColumns: [{ name: 'GRP_Id' }]
    }),
    __metadata("design:type", Array)
], User.prototype, "USE_Groups", void 0);
__decorate([
    typeorm_1.ManyToOne(type => role_entity_1.Role, role => role.ROL_Users),
    typeorm_1.JoinColumn({ name: "USE_Role" }),
    __metadata("design:type", role_entity_1.Role)
], User.prototype, "USE_Role", void 0);
__decorate([
    typeorm_1.BeforeInsert(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], User.prototype, "hashPassword", null);
__decorate([
    typeorm_1.BeforeInsert(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], User.prototype, "toLowerCase", null);
__decorate([
    typeorm_1.BeforeUpdate(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], User.prototype, "hashUpdatedPassword", null);
User = __decorate([
    typeorm_1.Entity('t_user')
], User);
exports.User = User;
//# sourceMappingURL=user.entity.js.map
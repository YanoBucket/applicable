import { SfpDto } from 'src/model/inventaire/sfp/sfp.dto';
import { SfpService } from './sfp.service';
import { CategoryDto } from 'src/model/inventaire/sfp/category.dto';
import { CategoryService } from './category.service';
export declare class SfpController {
    private sfpService;
    private categoryService;
    private logger;
    constructor(sfpService: SfpService, categoryService: CategoryService);
    findAllSfps(): Promise<SfpDto[]>;
    findAllCategories(): Promise<CategoryDto[]>;
    findById(id: string): Promise<SfpDto>;
    create(data: SfpDto): Promise<SfpDto>;
    createCategory(data: CategoryDto): Promise<CategoryDto>;
    update(id: string, data: Partial<SfpDto>): Promise<number>;
    updateConnector(id: string, data: Partial<CategoryDto>): Promise<number>;
    delete(id: string): Promise<number>;
    deleteCategory(id: string): Promise<number>;
}

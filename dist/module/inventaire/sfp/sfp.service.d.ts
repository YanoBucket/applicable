import { Repository } from 'typeorm';
import { SfpDto } from 'src/model/inventaire/sfp/sfp.dto';
import { Sfp } from 'src/entity/sfp/sfp.entity';
export declare class SfpService {
    private sfpsRepository;
    private NOT_FOUND;
    private logger;
    constructor(sfpsRepository: Repository<Sfp>);
    findAll(): Promise<SfpDto[]>;
    findById(id: string): Promise<SfpDto>;
    create(dto: SfpDto): Promise<SfpDto>;
    update(id: string, dto: Partial<SfpDto>): Promise<number>;
    delete(id: any): Promise<number>;
    private readById;
}

import { Repository } from 'typeorm';
import { Category } from '../../../entity/sfp/category.entity';
import { CategoryDto } from 'src/model/inventaire/sfp/category.dto';
export declare class CategoryService {
    private categoryRepository;
    private logger;
    private NOT_FOUND;
    private CATEGORY_NAME_TAKEN;
    private RESOURCE_BUSY;
    constructor(categoryRepository: Repository<Category>);
    findAll(): Promise<CategoryDto[]>;
    create(dto: CategoryDto): Promise<CategoryDto>;
    update(id: string, dto: Partial<CategoryDto>): Promise<number>;
    delete(id: any): Promise<number>;
    private readById;
}

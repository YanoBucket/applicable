"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const validation_pipe_1 = require("../../../common/validators/validation.pipe");
const sfp_dto_1 = require("../../../model/inventaire/sfp/sfp.dto");
const sfp_service_1 = require("./sfp.service");
const category_dto_1 = require("../../../model/inventaire/sfp/category.dto");
const category_service_1 = require("./category.service");
const passport_1 = require("@nestjs/passport");
const role_guard_1 = require("../../../common/security/role.guard");
const role_decorator_1 = require("../../../common/decorators/role.decorator");
let SfpController = class SfpController {
    constructor(sfpService, categoryService) {
        this.sfpService = sfpService;
        this.categoryService = categoryService;
        this.logger = new common_1.Logger("SfpController");
    }
    findAllSfps() {
        return this.sfpService.findAll();
    }
    findAllCategories() {
        return this.categoryService.findAll();
    }
    findById(id) {
        return this.sfpService.findById(id);
    }
    create(data) {
        this.logger.log(`Nouveau sfp : ${JSON.stringify(data)}`);
        return this.sfpService.create(new sfp_dto_1.SfpDto(data));
    }
    createCategory(data) {
        this.logger.log(`Nouvelle catégorie de SFP : ${JSON.stringify(data)}`);
        return this.categoryService.create(new category_dto_1.CategoryDto(data));
    }
    update(id, data) {
        this.logger.log(`Mise à jour : ${JSON.stringify(data)}`);
        return this.sfpService.update(id, new sfp_dto_1.SfpDto(data));
    }
    updateConnector(id, data) {
        return this.categoryService.update(id, new category_dto_1.CategoryDto(data));
    }
    delete(id) {
        return this.sfpService.delete(id);
    }
    deleteCategory(id) {
        return this.categoryService.delete(id);
    }
};
__decorate([
    common_1.Get(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], SfpController.prototype, "findAllSfps", null);
__decorate([
    common_1.Get('/categories'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], SfpController.prototype, "findAllCategories", null);
__decorate([
    common_1.Get(':id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], SfpController.prototype, "findById", null);
__decorate([
    common_1.Post(),
    common_1.UseGuards(passport_1.AuthGuard()),
    common_1.UsePipes(new validation_pipe_1.ValidationPipe()),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [sfp_dto_1.SfpDto]),
    __metadata("design:returntype", Promise)
], SfpController.prototype, "create", null);
__decorate([
    common_1.Post('/categories'),
    common_1.UseGuards(passport_1.AuthGuard(), role_guard_1.RolesGuard),
    common_1.UsePipes(new validation_pipe_1.ValidationPipe()),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [category_dto_1.CategoryDto]),
    __metadata("design:returntype", Promise)
], SfpController.prototype, "createCategory", null);
__decorate([
    common_1.Put(':id'),
    common_1.UseGuards(passport_1.AuthGuard()),
    common_1.UsePipes(new validation_pipe_1.ValidationPipe()),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], SfpController.prototype, "update", null);
__decorate([
    common_1.Put('/categories/:id'),
    common_1.UseGuards(passport_1.AuthGuard(), role_guard_1.RolesGuard),
    role_decorator_1.RoleDecorator('administrateur'),
    common_1.UsePipes(new validation_pipe_1.ValidationPipe()),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], SfpController.prototype, "updateConnector", null);
__decorate([
    common_1.Delete(':id'),
    common_1.UseGuards(passport_1.AuthGuard()),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], SfpController.prototype, "delete", null);
__decorate([
    common_1.Delete('/categories/:id'),
    common_1.UseGuards(passport_1.AuthGuard(), role_guard_1.RolesGuard),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], SfpController.prototype, "deleteCategory", null);
SfpController = __decorate([
    common_1.Controller('inventaire/sfps'),
    __metadata("design:paramtypes", [sfp_service_1.SfpService,
        category_service_1.CategoryService])
], SfpController);
exports.SfpController = SfpController;
//# sourceMappingURL=sfp.controller.js.map
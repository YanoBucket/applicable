"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const sfp_entity_1 = require("../../../entity/sfp/sfp.entity");
const sfp_controller_1 = require("./sfp.controller");
const sfp_service_1 = require("./sfp.service");
const typeorm_1 = require("@nestjs/typeorm");
const category_service_1 = require("./category.service");
const category_entity_1 = require("../../../entity/sfp/category.entity");
const auth_module_1 = require("../../auth/auth.module");
let SfpModule = class SfpModule {
};
SfpModule = __decorate([
    common_1.Module({
        imports: [
            typeorm_1.TypeOrmModule.forFeature([sfp_entity_1.Sfp, category_entity_1.Category]),
            auth_module_1.AuthModule
        ],
        controllers: [sfp_controller_1.SfpController],
        providers: [sfp_service_1.SfpService, category_service_1.CategoryService]
    })
], SfpModule);
exports.SfpModule = SfpModule;
//# sourceMappingURL=sfp.module.js.map
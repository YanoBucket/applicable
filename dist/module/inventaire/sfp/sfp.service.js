"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("typeorm");
const sfp_dto_1 = require("../../../model/inventaire/sfp/sfp.dto");
const sfp_entity_1 = require("../../../entity/sfp/sfp.entity");
const typeorm_2 = require("@nestjs/typeorm");
let SfpService = class SfpService {
    constructor(sfpsRepository) {
        this.sfpsRepository = sfpsRepository;
        this.NOT_FOUND = "SFP introuvable";
        this.logger = new common_1.Logger("SfpService");
    }
    async findAll() {
        return await this.sfpsRepository.find({
            relations: ["CAT", "GRP"]
        }).then(items => items.map(e => sfp_dto_1.SfpDto.fromEntity(e)));
    }
    async findById(id) {
        return await this.readById(id);
    }
    async create(dto) {
        const sfp = await this.sfpsRepository.save(dto.toEntity());
        return await this.readById(sfp.SFP_Id);
    }
    async update(id, dto) {
        const sfp = await this.readById(id);
        const stmt = await this.sfpsRepository.update({ SFP_Id: sfp.id }, dto.toEntity());
        return stmt.raw.changedRows;
    }
    async delete(id) {
        const sfp = await this.readById(id);
        const stmt = await this.sfpsRepository.delete({ SFP_Id: sfp.id });
        return stmt.raw.affectedRows;
    }
    async readById(id) {
        this.logger.log("TEST de : " + id);
        const sfp = await this.sfpsRepository
            .findOne({
            relations: ["CAT", "GRP", "GRP.TYP"],
            where: { SFP_Id: id }
        });
        if (!sfp) {
            throw new common_1.HttpException(this.NOT_FOUND, common_1.HttpStatus.NOT_FOUND);
        }
        return sfp_dto_1.SfpDto.fromEntity(sfp);
    }
};
SfpService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_2.InjectRepository(sfp_entity_1.Sfp)),
    __metadata("design:paramtypes", [typeorm_1.Repository])
], SfpService);
exports.SfpService = SfpService;
//# sourceMappingURL=sfp.service.js.map
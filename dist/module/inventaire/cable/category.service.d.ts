import { Repository } from 'typeorm';
import { Category } from '../../../entity/cable/category.entity';
import { CategoryDto } from 'src/model/inventaire/cable/category.dto';
export declare class CategoryService {
    private categoryRepository;
    private logger;
    private NOT_FOUND;
    private RESOURCE_BUSY;
    private CATEGORY_NAME_TAKEN;
    constructor(categoryRepository: Repository<Category>);
    findAll(): Promise<CategoryDto[]>;
    create(dto: CategoryDto): Promise<CategoryDto>;
    update(id: string, dto: Partial<CategoryDto>): Promise<number>;
    delete(id: any): Promise<number>;
    private readById;
}

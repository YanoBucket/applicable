"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const cable_controller_1 = require("./cable.controller");
const cable_service_1 = require("./cable.service");
const category_service_1 = require("./category.service");
const connector_service_1 = require("./connector.service");
const cable_entity_1 = require("../../../entity/cable/cable.entity");
const category_entity_1 = require("../../../entity/cable/category.entity");
const connector_entity_1 = require("../../../entity/cable/connector.entity");
const auth_module_1 = require("../../auth/auth.module");
let CableModule = class CableModule {
};
CableModule = __decorate([
    common_1.Module({
        imports: [
            typeorm_1.TypeOrmModule.forFeature([cable_entity_1.Cable, category_entity_1.Category, connector_entity_1.Connector]),
            auth_module_1.AuthModule
        ],
        controllers: [cable_controller_1.CableController],
        providers: [cable_service_1.CableService, category_service_1.CategoryService, connector_service_1.ConnectorService]
    })
], CableModule);
exports.CableModule = CableModule;
//# sourceMappingURL=cable.module.js.map
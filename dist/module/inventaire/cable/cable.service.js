"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const cable_entity_1 = require("../../../entity/cable/cable.entity");
const cable_dto_1 = require("../../../model/inventaire/cable/cable.dto");
const group_model_1 = require("../../../model/auth/group.model");
const connector_dto_1 = require("../../../model/inventaire/cable/connector.dto");
const connector_entity_1 = require("../../../entity/cable/connector.entity");
let CableService = class CableService {
    constructor(cablesRepository) {
        this.cablesRepository = cablesRepository;
        this.NOT_FOUND = "Cable introuvable";
        this.logger = new common_1.Logger("CableService");
    }
    async findAll() {
        return await this.cablesRepository.find({
            relations: ["CON_A", "CON_B", "CAT", "GRP", "GRP.TYP"],
            order: {
                CAB_Quantity: 'DESC'
            }
        }).then(items => items.map(e => cable_dto_1.CableDto.fromEntity(e)));
    }
    async findById(id) {
        return await this.readById(id);
    }
    async findByLengthFork(data) {
        return await this.cablesRepository.createQueryBuilder("cable")
            .innerJoinAndSelect("cable.CON_A", "connector_a")
            .innerJoinAndSelect("cable.CON_B", "connector_b")
            .innerJoinAndSelect("cable.CAT", "category")
            .innerJoinAndSelect("cable.GRP", "group")
            .where(`CAB_Length >= ${data.start}`)
            .andWhere(`CAB_Length <= ${data.end}`)
            .orderBy("CAB_Length")
            .getMany().then(items => items.map(e => cable_dto_1.CableDto.fromEntity(e)));
    }
    async create(dto) {
        const cable = await this.cablesRepository.save(dto.toEntity());
        return await this.readById(cable.CAB_Id);
    }
    async update(id, dto) {
        const cable = await this.readById(id);
        const stmt = await this.cablesRepository.update({ CAB_Id: cable.id }, dto.toEntity());
        return stmt.raw.changedRows;
    }
    async delete(id) {
        const cable = await this.readById(id);
        const stmt = await this.cablesRepository.delete({ CAB_Id: cable.id });
        return stmt.raw.affectedRows;
    }
    async readById(id) {
        const cable = await this.cablesRepository
            .findOne({
            relations: ["CON_A", "CON_B", "CAT", "GRP", "GRP.TYP"],
            where: { CAB_Id: id }
        });
        if (!cable) {
            throw new common_1.HttpException(this.NOT_FOUND, common_1.HttpStatus.NOT_FOUND);
        }
        return cable_dto_1.CableDto.fromEntity(cable);
    }
};
CableService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(cable_entity_1.Cable)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], CableService);
exports.CableService = CableService;
//# sourceMappingURL=cable.service.js.map
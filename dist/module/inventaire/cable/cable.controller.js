"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const validation_pipe_1 = require("../../../common/validators/validation.pipe");
const cable_service_1 = require("./cable.service");
const category_service_1 = require("./category.service");
const connector_service_1 = require("./connector.service");
const passport_1 = require("@nestjs/passport");
const role_guard_1 = require("../../../common/security/role.guard");
const role_decorator_1 = require("../../../common/decorators/role.decorator");
const category_dto_1 = require("../../../model/inventaire/cable/category.dto");
const connector_dto_1 = require("../../../model/inventaire/cable/connector.dto");
const cable_dto_1 = require("../../../model/inventaire/cable/cable.dto");
let CableController = class CableController {
    constructor(cableService, categoryService, connectorService) {
        this.cableService = cableService;
        this.categoryService = categoryService;
        this.connectorService = connectorService;
        this.logger = new common_1.Logger("CableController");
    }
    findAllCables() {
        return this.cableService.findAll();
    }
    findAllCategories() {
        return this.categoryService.findAll();
    }
    findAllConnectors() {
        return this.connectorService.findAll();
    }
    findById(id) {
        return this.cableService.findById(id);
    }
    getCablesByLength(data) {
        return this.cableService.findByLengthFork(data);
    }
    create(data) {
        this.logger.log(`Nouveau cable : ${JSON.stringify(data)}`);
        return this.cableService.create(new cable_dto_1.CableDto(data));
    }
    createConnector(data) {
        return this.connectorService.create(new connector_dto_1.ConnectorDto(data));
    }
    createCategory(data) {
        return this.categoryService.create(new category_dto_1.CategoryDto(data));
    }
    update(id, data) {
        return this.cableService.update(id, new cable_dto_1.CableDto(data));
    }
    updateCategory(id, data) {
        return this.connectorService.update(id, new connector_dto_1.ConnectorDto(data));
    }
    updateConnector(id, data) {
        return this.categoryService.update(id, new category_dto_1.CategoryDto(data));
    }
    delete(id) {
        return this.cableService.delete(id);
    }
    deleteConnector(id) {
        return this.connectorService.delete(id);
    }
    deleteCategory(id) {
        return this.categoryService.delete(id);
    }
};
__decorate([
    common_1.Get(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], CableController.prototype, "findAllCables", null);
__decorate([
    common_1.Get('/categories'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], CableController.prototype, "findAllCategories", null);
__decorate([
    common_1.Get('/connectors'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], CableController.prototype, "findAllConnectors", null);
__decorate([
    common_1.Get(':id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], CableController.prototype, "findById", null);
__decorate([
    common_1.Post('/fork'),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], CableController.prototype, "getCablesByLength", null);
__decorate([
    common_1.Post(),
    common_1.UseGuards(passport_1.AuthGuard()),
    common_1.UsePipes(new validation_pipe_1.ValidationPipe()),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [cable_dto_1.CableDto]),
    __metadata("design:returntype", Promise)
], CableController.prototype, "create", null);
__decorate([
    common_1.Post('/connectors'),
    common_1.UseGuards(passport_1.AuthGuard(), role_guard_1.RolesGuard, role_guard_1.RolesGuard),
    role_decorator_1.RoleDecorator('administrateur'),
    common_1.UsePipes(new validation_pipe_1.ValidationPipe()),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [connector_dto_1.ConnectorDto]),
    __metadata("design:returntype", Promise)
], CableController.prototype, "createConnector", null);
__decorate([
    common_1.Post('/categories'),
    common_1.UseGuards(passport_1.AuthGuard(), role_guard_1.RolesGuard),
    role_decorator_1.RoleDecorator('administrateur'),
    common_1.UsePipes(new validation_pipe_1.ValidationPipe()),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [category_dto_1.CategoryDto]),
    __metadata("design:returntype", Promise)
], CableController.prototype, "createCategory", null);
__decorate([
    common_1.Put(':id'),
    common_1.UseGuards(passport_1.AuthGuard()),
    common_1.UsePipes(new validation_pipe_1.ValidationPipe()),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], CableController.prototype, "update", null);
__decorate([
    common_1.Put('/connectors/:id'),
    common_1.UseGuards(passport_1.AuthGuard(), role_guard_1.RolesGuard),
    role_decorator_1.RoleDecorator('administrateur'),
    common_1.UsePipes(new validation_pipe_1.ValidationPipe()),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], CableController.prototype, "updateCategory", null);
__decorate([
    common_1.Put('/categories/:id'),
    common_1.UseGuards(passport_1.AuthGuard(), role_guard_1.RolesGuard),
    role_decorator_1.RoleDecorator('administrateur'),
    common_1.UsePipes(new validation_pipe_1.ValidationPipe()),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], CableController.prototype, "updateConnector", null);
__decorate([
    common_1.Delete(':id'),
    common_1.UseGuards(passport_1.AuthGuard()),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], CableController.prototype, "delete", null);
__decorate([
    common_1.Delete('/connectors/:id'),
    common_1.UseGuards(passport_1.AuthGuard(), role_guard_1.RolesGuard),
    role_decorator_1.RoleDecorator('administrateur'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], CableController.prototype, "deleteConnector", null);
__decorate([
    common_1.Delete('/categories/:id'),
    common_1.UseGuards(passport_1.AuthGuard(), role_guard_1.RolesGuard),
    role_decorator_1.RoleDecorator('administrateur'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], CableController.prototype, "deleteCategory", null);
CableController = __decorate([
    common_1.Controller('inventaire/cables'),
    __metadata("design:paramtypes", [cable_service_1.CableService,
        category_service_1.CategoryService,
        connector_service_1.ConnectorService])
], CableController);
exports.CableController = CableController;
//# sourceMappingURL=cable.controller.js.map
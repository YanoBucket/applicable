import { Repository } from 'typeorm';
import { Cable } from '../../../entity/cable/cable.entity';
import { CableDto } from '../../../model/inventaire/cable/cable.dto';
export declare class CableService {
    private cablesRepository;
    private NOT_FOUND;
    private logger;
    constructor(cablesRepository: Repository<Cable>);
    findAll(): Promise<CableDto[]>;
    findById(id: string): Promise<CableDto>;
    findByLengthFork(data: any): Promise<CableDto[]>;
    create(dto: CableDto): Promise<CableDto>;
    update(id: string, dto: Partial<CableDto>): Promise<number>;
    delete(id: any): Promise<number>;
    private readById;
}

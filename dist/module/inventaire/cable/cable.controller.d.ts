import { CableService } from './cable.service';
import { CategoryService } from './category.service';
import { ConnectorService } from './connector.service';
import { CategoryDto } from 'src/model/inventaire/cable/category.dto';
import { ConnectorDto } from 'src/model/inventaire/cable/connector.dto';
import { CableDto } from '../../../model/inventaire/cable/cable.dto';
export declare class CableController {
    private cableService;
    private categoryService;
    private connectorService;
    private logger;
    constructor(cableService: CableService, categoryService: CategoryService, connectorService: ConnectorService);
    findAllCables(): Promise<CableDto[]>;
    findAllCategories(): Promise<CategoryDto[]>;
    findAllConnectors(): Promise<ConnectorDto[]>;
    findById(id: string): Promise<CableDto>;
    getCablesByLength(data: {
        start: any;
        end: any;
    }): Promise<CableDto[]>;
    create(data: CableDto): Promise<CableDto>;
    createConnector(data: ConnectorDto): Promise<ConnectorDto>;
    createCategory(data: CategoryDto): Promise<CategoryDto>;
    update(id: string, data: Partial<CableDto>): Promise<number>;
    updateCategory(id: string, data: Partial<ConnectorDto>): Promise<number>;
    updateConnector(id: string, data: Partial<CategoryDto>): Promise<number>;
    delete(id: string): Promise<number>;
    deleteConnector(id: string): Promise<number>;
    deleteCategory(id: string): Promise<number>;
}

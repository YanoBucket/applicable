import { Repository } from 'typeorm';
import { ConnectorDto } from 'src/model/inventaire/cable/connector.dto';
import { Connector } from 'src/entity/cable/connector.entity';
export declare class ConnectorService {
    private connectorRepository;
    private logger;
    private NOT_FOUND;
    private CONNECTOR_NAME_TAKEN;
    private RESOURCE_BUSY;
    constructor(connectorRepository: Repository<Connector>);
    findAll(): Promise<ConnectorDto[]>;
    create(dto: ConnectorDto): Promise<ConnectorDto>;
    update(id: string, dto: Partial<ConnectorDto>): Promise<number>;
    delete(id: any): Promise<number>;
    private readById;
}

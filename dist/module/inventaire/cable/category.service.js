"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const category_entity_1 = require("../../../entity/cable/category.entity");
const category_dto_1 = require("../../../model/inventaire/cable/category.dto");
let CategoryService = class CategoryService {
    constructor(categoryRepository) {
        this.categoryRepository = categoryRepository;
        this.logger = new common_1.Logger("CategoryService");
        this.NOT_FOUND = "Categorie introuvable";
        this.RESOURCE_BUSY = 'Supprimer les ressources liées à cette catégorie pour pouvoir la supprimer';
        this.CATEGORY_NAME_TAKEN = "Ce nom de catégorie existe déjà";
    }
    async findAll() {
        return await this.categoryRepository.find({
            order: {
                CAB_CAT_Name: 'ASC'
            }
        })
            .then(items => items.map(e => category_dto_1.CategoryDto.fromEntity(e)));
    }
    async create(dto) {
        try {
            const category = await this.categoryRepository.save(dto.toEntity());
            return await this.readById(category.CAB_CAT_Id);
        }
        catch (err) {
            if (err.errno == '1062') {
                throw new common_1.ConflictException(this.CATEGORY_NAME_TAKEN);
            }
            throw err;
        }
    }
    async update(id, dto) {
        try {
            const category = await this.readById(id);
            const stmt = await this.categoryRepository.update({ CAB_CAT_Id: category.id }, dto.toEntity());
            return stmt.raw.changedRows;
        }
        catch (err) {
            if (err.errno == '1062') {
                throw new common_1.ConflictException(this.CATEGORY_NAME_TAKEN);
            }
            throw err;
        }
    }
    async delete(id) {
        try {
            const category = await this.readById(id);
            const stmt = await this.categoryRepository.delete({ CAB_CAT_Id: category.id });
            return stmt.raw.affectedRows;
        }
        catch (err) {
            if (err.errno == '1451') {
                throw new common_1.ConflictException(this.RESOURCE_BUSY);
            }
            throw err;
        }
    }
    async readById(id) {
        const category = await this.categoryRepository
            .findOne({
            where: { CAB_CAT_Id: id }
        });
        if (!category) {
            throw new common_1.HttpException(this.NOT_FOUND, common_1.HttpStatus.NOT_FOUND);
        }
        return category_dto_1.CategoryDto.fromEntity(category);
    }
};
CategoryService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(category_entity_1.Category)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], CategoryService);
exports.CategoryService = CategoryService;
//# sourceMappingURL=category.service.js.map
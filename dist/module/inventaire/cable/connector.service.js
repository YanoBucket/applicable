"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const connector_dto_1 = require("../../../model/inventaire/cable/connector.dto");
const connector_entity_1 = require("../../../entity/cable/connector.entity");
let ConnectorService = class ConnectorService {
    constructor(connectorRepository) {
        this.connectorRepository = connectorRepository;
        this.logger = new common_1.Logger("ConnectorService");
        this.NOT_FOUND = "Connecteur introuvable";
        this.CONNECTOR_NAME_TAKEN = "Ce nom de connecteur existe déjà";
        this.RESOURCE_BUSY = 'Supprimer les ressources liées à ce connecteur pour pouvoir le supprimer';
    }
    async findAll() {
        return await this.connectorRepository.find()
            .then(items => items.map(e => connector_dto_1.ConnectorDto.fromEntity(e)));
    }
    async create(dto) {
        try {
            const connector = await this.connectorRepository.save(dto.toEntity());
            return await this.readById(connector.CON_Id);
        }
        catch (err) {
            if (err.errno == '1062') {
                throw new common_1.ConflictException(this.CONNECTOR_NAME_TAKEN);
            }
            throw err;
        }
    }
    async update(id, dto) {
        try {
            const connector = await this.readById(id);
            const stmt = await this.connectorRepository.update({ CON_Id: connector.id }, dto.toEntity());
            return stmt.raw.changedRows;
        }
        catch (err) {
            if (err.errno == '1062') {
                throw new common_1.ConflictException(this.CONNECTOR_NAME_TAKEN);
            }
            throw err;
        }
    }
    async delete(id) {
        try {
            const connector = await this.readById(id);
            const stmt = await this.connectorRepository.delete({ CON_Id: connector.id });
            return stmt.raw.affectedRows;
        }
        catch (err) {
            if (err.errno == '1451') {
                throw new common_1.ConflictException(this.RESOURCE_BUSY);
            }
            throw err;
        }
    }
    async readById(id) {
        const connector = await this.connectorRepository
            .findOne({
            where: { CON_Id: id }
        });
        if (!connector) {
            throw new common_1.HttpException(this.NOT_FOUND, common_1.HttpStatus.NOT_FOUND);
        }
        return connector_dto_1.ConnectorDto.fromEntity(connector);
    }
};
ConnectorService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(connector_entity_1.Connector)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], ConnectorService);
exports.ConnectorService = ConnectorService;
//# sourceMappingURL=connector.service.js.map
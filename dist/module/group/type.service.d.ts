import { Type } from 'src/entity/auth/type.entity';
import { Repository } from 'typeorm';
import { TypeDto } from 'src/model/auth/type.model';
export declare class TypeService {
    private typeRepository;
    private NOT_FOUND;
    private logger;
    constructor(typeRepository: Repository<Type>);
    findAll(): Promise<TypeDto[]>;
}

import { GroupDto } from 'src/model/auth/group.model';
import { Group } from 'src/entity/auth/group.entity';
import { Repository } from 'typeorm';
export declare class GroupService {
    private groupeRepository;
    private NOT_FOUND;
    private GROUP_NAME_TAKEN;
    private RESOURCE_BUSY;
    private logger;
    constructor(groupeRepository: Repository<Group>);
    findAll(): Promise<GroupDto[]>;
    findAllByGroup(type: string): Promise<any[]>;
    create(dto: GroupDto): Promise<GroupDto>;
    update(id: string, dto: Partial<GroupDto>): Promise<number>;
    delete(id: any): Promise<number>;
    private readById;
}

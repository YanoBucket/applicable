"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const group_service_1 = require("./group.service");
const group_model_1 = require("../../model/auth/group.model");
const role_guard_1 = require("../../common/security/role.guard");
const passport_1 = require("@nestjs/passport");
const role_decorator_1 = require("../../common/decorators/role.decorator");
const type_model_1 = require("../../model/auth/type.model");
const type_service_1 = require("./type.service");
let GroupController = class GroupController {
    constructor(groupService, typeService) {
        this.groupService = groupService;
        this.typeService = typeService;
        this.logger = new common_1.Logger("GroupController");
    }
    findAllGroups() {
        return this.groupService.findAll();
    }
    findAllTypes() {
        return this.typeService.findAll();
    }
    findAllGroupsInventaire(type) {
        return this.groupService.findAllByGroup(type);
    }
    create(data) {
        this.logger.log(`Nouveau groupe : ${JSON.stringify(data)}`);
        return this.groupService.create(new group_model_1.GroupDto(data));
    }
    update(id, data) {
        this.logger.log(`Mise à jour : ${JSON.stringify(data)} sur le groupe d'identifiant : ${id}`);
        return this.groupService.update(id, new group_model_1.GroupDto(data));
    }
    delete(id) {
        return this.groupService.delete(id);
    }
};
__decorate([
    common_1.Get(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], GroupController.prototype, "findAllGroups", null);
__decorate([
    common_1.Get('/types'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], GroupController.prototype, "findAllTypes", null);
__decorate([
    common_1.Get(':type'),
    __param(0, common_1.Param('type')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], GroupController.prototype, "findAllGroupsInventaire", null);
__decorate([
    common_1.Post(),
    common_1.UseGuards(passport_1.AuthGuard(), role_guard_1.RolesGuard),
    role_decorator_1.RoleDecorator('administrateur'),
    common_1.UsePipes(new common_1.ValidationPipe()),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [group_model_1.GroupDto]),
    __metadata("design:returntype", Promise)
], GroupController.prototype, "create", null);
__decorate([
    common_1.Put(':id'),
    common_1.UseGuards(passport_1.AuthGuard(), role_guard_1.RolesGuard),
    role_decorator_1.RoleDecorator('administrateur'),
    common_1.UsePipes(new common_1.ValidationPipe()),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], GroupController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    common_1.UseGuards(passport_1.AuthGuard(), role_guard_1.RolesGuard),
    role_decorator_1.RoleDecorator('administrateur'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], GroupController.prototype, "delete", null);
GroupController = __decorate([
    common_1.Controller('groups'),
    __metadata("design:paramtypes", [group_service_1.GroupService,
        type_service_1.TypeService])
], GroupController);
exports.GroupController = GroupController;
//# sourceMappingURL=group.controller.js.map
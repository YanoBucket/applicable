"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const group_model_1 = require("../../model/auth/group.model");
const typeorm_1 = require("@nestjs/typeorm");
const group_entity_1 = require("../../entity/auth/group.entity");
const typeorm_2 = require("typeorm");
let GroupService = class GroupService {
    constructor(groupeRepository) {
        this.groupeRepository = groupeRepository;
        this.NOT_FOUND = "Groupe introuvable";
        this.GROUP_NAME_TAKEN = 'Ce nom de groupe existe déjà';
        this.RESOURCE_BUSY = 'Supprimer les ressources liées à ce groupe pour pouvoir le supprimer';
        this.logger = new common_1.Logger("GroupService");
    }
    async findAll() {
        return await this.groupeRepository.find({
            relations: ['TYP'],
            order: {
                TYP: 'ASC',
                GRP_Name: 'ASC'
            }
        }).then(items => items.map(e => group_model_1.GroupDto.fromEntity(e)));
    }
    async findAllByGroup(type) {
        return await this.groupeRepository.createQueryBuilder("group")
            .select("group.GRP_Name", "GRP_Name")
            .addSelect("group.GRP_Id", "GRP_Id")
            .innerJoin("group.TYP", "type")
            .where("type.GRP_TYP_Name = :name", { name: type })
            .getRawMany().then(items => items.map(e => group_model_1.GroupDto.fromEntity(e)));
    }
    async create(dto) {
        try {
            const group = await this.groupeRepository.save(dto.toEntity());
            return await this.readById(group.GRP_Id);
        }
        catch (err) {
            if (err.errno == '1062') {
                throw new common_1.ConflictException(this.GROUP_NAME_TAKEN);
            }
            throw err;
        }
    }
    async update(id, dto) {
        try {
            const cable = await this.readById(id);
            const stmt = await this.groupeRepository.update({ GRP_Id: cable.id }, dto.toEntity());
            return stmt.raw.changedRows;
        }
        catch (err) {
            if (err.errno == '1062') {
                throw new common_1.ConflictException(this.GROUP_NAME_TAKEN);
            }
            throw err;
        }
    }
    async delete(id) {
        try {
            const group = await this.readById(id);
            const stmt = await this.groupeRepository.delete({ GRP_Id: group.id });
            return stmt.raw.affectedRows;
        }
        catch (err) {
            if (err.errno == '1451') {
                throw new common_1.ConflictException(this.RESOURCE_BUSY);
            }
            throw err;
        }
    }
    async readById(id) {
        const group = await this.groupeRepository
            .findOne({
            relations: ["TYP"],
            where: { GRP_Id: id }
        });
        if (!group) {
            throw new common_1.HttpException(this.NOT_FOUND, common_1.HttpStatus.NOT_FOUND);
        }
        return group_model_1.GroupDto.fromEntity(group);
    }
};
GroupService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(group_entity_1.Group)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], GroupService);
exports.GroupService = GroupService;
//# sourceMappingURL=group.service.js.map
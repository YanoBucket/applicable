import { GroupService } from './group.service';
import { GroupDto } from 'src/model/auth/group.model';
import { TypeDto } from 'src/model/auth/type.model';
import { TypeService } from './type.service';
export declare class GroupController {
    private groupService;
    private typeService;
    private logger;
    constructor(groupService: GroupService, typeService: TypeService);
    findAllGroups(): Promise<GroupDto[]>;
    findAllTypes(): Promise<TypeDto[]>;
    findAllGroupsInventaire(type: string): Promise<any[]>;
    create(data: GroupDto): Promise<GroupDto>;
    update(id: string, data: Partial<GroupDto>): Promise<number>;
    delete(id: string): Promise<number>;
}

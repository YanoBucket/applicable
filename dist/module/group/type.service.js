"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const type_entity_1 = require("../../entity/auth/type.entity");
const typeorm_2 = require("typeorm");
const type_model_1 = require("../../model/auth/type.model");
let TypeService = class TypeService {
    constructor(typeRepository) {
        this.typeRepository = typeRepository;
        this.NOT_FOUND = "Type introuvable";
        this.logger = new common_1.Logger("TypeService");
    }
    async findAll() {
        return await this.typeRepository.find({
            order: { GRP_TYP_Name: 'DESC' }
        }).then(items => items.map(e => type_model_1.TypeDto.fromEntity(e)));
    }
};
TypeService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(type_entity_1.Type)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], TypeService);
exports.TypeService = TypeService;
//# sourceMappingURL=type.service.js.map
import { Repository } from 'typeorm';
import { User } from 'src/entity/auth/user.entity';
import { UserDto } from 'src/model/auth/user.model';
import { Group } from 'src/entity/auth/group.entity';
import { Role } from 'src/entity/auth/role.entity';
export declare class UserService {
    private userRepository;
    private groupRepository;
    private roleRepository;
    private NOT_FOUND;
    private NOT_PERMITTED;
    private logger;
    constructor(userRepository: Repository<User>, groupRepository: Repository<Group>, roleRepository: Repository<Role>);
    findAll(): Promise<UserDto[]>;
    update(id: string, dto: Partial<UserDto>): Promise<number>;
    reset(id: string, dto: Partial<UserDto>): Promise<number>;
    delete(id: any): Promise<number>;
    private updateRoleGroups;
    private isEqualGroups;
    readById(id: string): Promise<UserDto>;
}

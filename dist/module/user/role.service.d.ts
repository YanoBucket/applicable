import { Repository } from 'typeorm';
import { Role } from 'src/entity/auth/role.entity';
import { RoleDto } from 'src/model/auth/role.model';
export declare class RoleService {
    private roleRepository;
    private NOT_FOUND;
    private logger;
    constructor(roleRepository: Repository<Role>);
    findAll(): Promise<RoleDto[]>;
    private readById;
}

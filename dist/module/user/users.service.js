"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("typeorm");
const user_entity_1 = require("../../entity/auth/user.entity");
const typeorm_2 = require("@nestjs/typeorm");
const user_model_1 = require("../../model/auth/user.model");
const group_entity_1 = require("../../entity/auth/group.entity");
const group_model_1 = require("../../model/auth/group.model");
const role_model_1 = require("../../model/auth/role.model");
const role_entity_1 = require("../../entity/auth/role.entity");
let UserService = class UserService {
    constructor(userRepository, groupRepository, roleRepository) {
        this.userRepository = userRepository;
        this.groupRepository = groupRepository;
        this.roleRepository = roleRepository;
        this.NOT_FOUND = 'Utilisateur introuvable';
        this.NOT_PERMITTED = 'Uniquement le rôle et les groupes de l\'utilisateurs sont modifiable';
        this.logger = new common_1.Logger('UserService');
    }
    async findAll() {
        return await this.userRepository.find({
            relations: ["USE_Role", "USE_Groups"],
            order: {
                USE_Role: 'ASC',
                USE_Username: 'ASC'
            }
        }).then(items => items.map(e => user_model_1.UserDto.fromEntity(e)));
    }
    async update(id, dto) {
        let user = await this.readById(id);
        const stmt = await this.updateRoleGroups(dto, user);
        if (stmt === 0) {
            throw new common_1.BadRequestException(this.NOT_PERMITTED);
        }
        return 1;
    }
    async reset(id, dto) {
        let user = await this.readById(id);
        const userUpdated = user.toEntity();
        userUpdated.USE_Password = dto.password;
        const stmt = await this.userRepository.save(userUpdated);
        return 1;
    }
    async delete(id) {
        const user = await this.readById(id);
        const stmt = await this.userRepository.delete({ USE_Id: user.id });
        return stmt.raw.affectedRows;
    }
    async updateRoleGroups(dto, user) {
        let process = new Promise((resolve, reject) => {
            if (dto.groups || (dto.role && (dto.role.id !== user.role.id))) {
                if (dto.groups) {
                    user.groups = [];
                    let promiseGroup = dto.groups.map(async (group) => {
                        let grp = await this.groupRepository.findOne(group.id);
                        if (grp) {
                            return group_model_1.GroupDto.fromEntity(grp);
                        }
                    });
                    Promise.all(promiseGroup).then((groups) => {
                        groups.forEach(grp => {
                            if (grp) {
                                user.groups.push(grp);
                            }
                        });
                    }).finally(() => {
                        if (user.groups) {
                            this.userRepository.save(user.toEntity());
                            resolve(1);
                        }
                        else {
                            resolve(0);
                        }
                    });
                }
                if (dto.role && (dto.role.id !== user.role.id)) {
                    let promiseRole = this.roleRepository.findOne(dto.role.id).then(async (result) => {
                        if (result) {
                            return result;
                        }
                    });
                    promiseRole.then((role) => {
                        if (role) {
                            this.userRepository.createQueryBuilder()
                                .update()
                                .set({ USE_Role: role })
                                .where("USE_Id = :id", { id: user.id })
                                .execute();
                            resolve(1);
                        }
                        else {
                            resolve(0);
                        }
                    });
                }
            }
            else {
                resolve(0);
            }
        });
        return process.then((result) => {
            return result ? 1 : 0;
        });
    }
    isEqualGroups(user, dto) {
        if (user.length !== dto.length)
            return false;
        let equal = true;
        user.forEach(grp => {
        });
        return equal;
    }
    async readById(id) {
        const user = await this.userRepository
            .findOne({
            relations: ["USE_Role", "USE_Groups"],
            where: { USE_Id: id }
        });
        if (!user) {
            throw new common_1.HttpException(this.NOT_FOUND, common_1.HttpStatus.NOT_FOUND);
        }
        return user_model_1.UserDto.fromEntity(user);
    }
};
UserService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_2.InjectRepository(user_entity_1.User)),
    __param(1, typeorm_2.InjectRepository(group_entity_1.Group)),
    __param(2, typeorm_2.InjectRepository(role_entity_1.Role)),
    __metadata("design:paramtypes", [typeorm_1.Repository,
        typeorm_1.Repository,
        typeorm_1.Repository])
], UserService);
exports.UserService = UserService;
//# sourceMappingURL=users.service.js.map
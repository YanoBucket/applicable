import { UserService } from './users.service';
import { UserDto } from 'src/model/auth/user.model';
import { AuthService } from '../auth/auth.service';
import { RoleService } from './role.service';
import { RoleDto } from 'src/model/auth/role.model';
export declare class UserController {
    private userService;
    private authService;
    private roleService;
    private logger;
    constructor(userService: UserService, authService: AuthService, roleService: RoleService);
    findAll(): Promise<UserDto[]>;
    findAllRoles(): Promise<RoleDto[]>;
    register(data: UserDto): Promise<UserDto>;
    update(id: string, data: Partial<UserDto>): Promise<number>;
    reset(id: string, data: Partial<UserDto>): Promise<number>;
    delete(id: string): Promise<number>;
}

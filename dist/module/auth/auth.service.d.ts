import { Repository } from 'typeorm';
import { User } from 'src/entity/auth/user.entity';
import { JwtService } from '@nestjs/jwt';
import { UserDto } from 'src/model/auth/user.model';
export declare class AuthService {
    private userRepository;
    private jwtService;
    private NOT_FOUND;
    private USERNAME_TAKEN;
    private INVALIDE_PASSWORD;
    private logger;
    constructor(userRepository: Repository<User>, jwtService: JwtService);
    register(dto: UserDto): Promise<UserDto>;
    login(dto: UserDto): Promise<{
        user: UserDto;
        token: string;
    }>;
    updatePassword(dto: any, currentUser: any, currentPassword: any): Promise<number>;
    private buildToken;
    private readByUsername;
}

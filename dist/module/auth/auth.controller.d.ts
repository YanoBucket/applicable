import { AuthService } from './auth.service';
import { UserDto } from 'src/model/auth/user.model';
import { User } from 'src/entity/auth/user.entity';
export declare class AuthController {
    private authService;
    private logger;
    constructor(authService: AuthService);
    login(credentials: UserDto): Promise<{
        user: UserDto;
        token: string;
    }>;
    updatePassword(user: UserDto, currentPassword: string, currentUser: User): Promise<number>;
}

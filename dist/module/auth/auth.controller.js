"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const auth_service_1 = require("./auth.service");
const user_model_1 = require("../../model/auth/user.model");
const passport_1 = require("@nestjs/passport");
const user_decorator_1 = require("../../common/decorators/user.decorator");
const user_entity_1 = require("../../entity/auth/user.entity");
let AuthController = class AuthController {
    constructor(authService) {
        this.authService = authService;
        this.logger = new common_1.Logger('authController');
    }
    login(credentials) {
        return this.authService.login(new user_model_1.UserDto(credentials));
    }
    updatePassword(user, currentPassword, currentUser) {
        return this.authService.updatePassword(new user_model_1.UserDto(user), currentUser, currentPassword);
    }
};
__decorate([
    common_1.Post('/login'),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_model_1.UserDto]),
    __metadata("design:returntype", void 0)
], AuthController.prototype, "login", null);
__decorate([
    common_1.Put(),
    common_1.UseGuards(passport_1.AuthGuard()),
    common_1.UsePipes(new common_1.ValidationPipe()),
    __param(0, common_1.Body('user')),
    __param(1, common_1.Body('currentPassword')),
    __param(2, user_decorator_1.UserDecorator()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [user_model_1.UserDto, String, user_entity_1.User]),
    __metadata("design:returntype", void 0)
], AuthController.prototype, "updatePassword", null);
AuthController = __decorate([
    common_1.Controller('auth'),
    __metadata("design:paramtypes", [auth_service_1.AuthService])
], AuthController);
exports.AuthController = AuthController;
//# sourceMappingURL=auth.controller.js.map
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("typeorm");
const user_entity_1 = require("../../entity/auth/user.entity");
const jwt_1 = require("@nestjs/jwt");
const typeorm_2 = require("@nestjs/typeorm");
const user_model_1 = require("../../model/auth/user.model");
let AuthService = class AuthService {
    constructor(userRepository, jwtService) {
        this.userRepository = userRepository;
        this.jwtService = jwtService;
        this.NOT_FOUND = 'Utilisateur introuvable';
        this.USERNAME_TAKEN = 'Ce nom d\'utilisateur existe déjà';
        this.INVALIDE_PASSWORD = 'Mot de passe invalide';
        this.logger = new common_1.Logger('AuthService');
    }
    async register(dto) {
        try {
            const user = await this.userRepository.save(dto.toEntity());
            return user_model_1.UserDto.fromEntity(user);
        }
        catch (err) {
            if (err.errno == '1062') {
                throw new common_1.ConflictException(this.USERNAME_TAKEN);
            }
            throw err;
        }
    }
    async login(dto) {
        try {
            const user = await this.readByUsername(dto.username);
            const isValid = await user.comparePassword(dto.password);
            if (!isValid) {
                throw new common_1.NotFoundException(this.INVALIDE_PASSWORD);
            }
            this.logger.log(`Sign token with ${user.USE_Id}`);
            const token = this.buildToken(user.USE_Id);
            return { user: user_model_1.UserDto.fromEntity(user), token };
        }
        catch (err) {
            throw err;
        }
    }
    async updatePassword(dto, currentUser, currentPassword) {
        const user = await this.readByUsername(currentUser.username);
        const isValid = await user.comparePassword(currentPassword);
        if (!isValid) {
            throw new common_1.NotFoundException(this.INVALIDE_PASSWORD);
        }
        const userUpdated = currentUser.toEntity();
        userUpdated.USE_Password = dto.password;
        return this.userRepository.save(userUpdated);
    }
    buildToken(id) {
        return this.jwtService.sign({}, {
            expiresIn: "3d",
            subject: id
        });
    }
    async readByUsername(username) {
        const user = await this.userRepository
            .findOne({
            relations: ["USE_Role", "USE_Groups"],
            where: { USE_Username: username }
        });
        if (!user) {
            throw new common_1.HttpException(this.NOT_FOUND, common_1.HttpStatus.NOT_FOUND);
        }
        return user;
    }
};
AuthService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_2.InjectRepository(user_entity_1.User)),
    __metadata("design:paramtypes", [typeorm_1.Repository,
        jwt_1.JwtService])
], AuthService);
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map
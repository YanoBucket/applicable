import { PathService } from './path.service';
import { PathDto } from 'src/model/calcul/path.model';
import { GroupDto } from 'src/model/auth/group.model';
export declare class PathController {
    private pathService;
    private logger;
    constructor(pathService: PathService);
    findAllPath(): Promise<PathDto[]>;
    findAllByGroup(data: Partial<GroupDto>): Promise<PathDto[]>;
    create(data: PathDto): Promise<PathDto>;
    update(id: string, data: Partial<PathDto>): Promise<number>;
    delete(id: string): Promise<number>;
}

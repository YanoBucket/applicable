"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const path_service_1 = require("./path.service");
const path_model_1 = require("../../../model/calcul/path.model");
const group_model_1 = require("../../../model/auth/group.model");
const passport_1 = require("@nestjs/passport");
const role_guard_1 = require("../../../common/security/role.guard");
const role_decorator_1 = require("../../../common/decorators/role.decorator");
let PathController = class PathController {
    constructor(pathService) {
        this.pathService = pathService;
        this.logger = new common_1.Logger("PathController");
    }
    findAllPath() {
        return this.pathService.findAll();
    }
    findAllByGroup(data) {
        return this.pathService.findAllByGroup(new group_model_1.GroupDto(data));
    }
    create(data) {
        this.logger.log(`Nouveau chemin : ${JSON.stringify(data)}`);
        return this.pathService.create(new path_model_1.PathDto(data));
    }
    update(id, data) {
        this.logger.log(`Mise à jour : ${JSON.stringify(data)} sur le chemin d'identifiant : ${id}`);
        return this.pathService.update(id, new path_model_1.PathDto(data));
    }
    delete(id) {
        return this.pathService.delete(id);
    }
};
__decorate([
    common_1.Get(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], PathController.prototype, "findAllPath", null);
__decorate([
    common_1.Get('group'),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], PathController.prototype, "findAllByGroup", null);
__decorate([
    common_1.Post(),
    common_1.UseGuards(passport_1.AuthGuard(), role_guard_1.RolesGuard),
    role_decorator_1.RoleDecorator('administrateur'),
    common_1.UsePipes(new common_1.ValidationPipe()),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [path_model_1.PathDto]),
    __metadata("design:returntype", Promise)
], PathController.prototype, "create", null);
__decorate([
    common_1.Put(':id'),
    common_1.UseGuards(passport_1.AuthGuard(), role_guard_1.RolesGuard),
    role_decorator_1.RoleDecorator('administrateur'),
    common_1.UsePipes(new common_1.ValidationPipe()),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], PathController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    common_1.UseGuards(passport_1.AuthGuard(), role_guard_1.RolesGuard),
    role_decorator_1.RoleDecorator('administrateur'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], PathController.prototype, "delete", null);
PathController = __decorate([
    common_1.Controller('path'),
    __metadata("design:paramtypes", [path_service_1.PathService])
], PathController);
exports.PathController = PathController;
//# sourceMappingURL=path.controller.js.map
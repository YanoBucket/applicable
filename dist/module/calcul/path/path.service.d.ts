import { Path } from 'src/entity/calcul/path.entity';
import { Repository } from 'typeorm';
import { PathDto } from 'src/model/calcul/path.model';
import { GroupDto } from 'src/model/auth/group.model';
export declare class PathService {
    private pathRepository;
    private NOT_FOUND;
    private COMP_TAKEN;
    private logger;
    constructor(pathRepository: Repository<Path>);
    findAll(): Promise<PathDto[]>;
    findAllByGroup(dto: GroupDto): Promise<PathDto[]>;
    create(dto: PathDto): Promise<PathDto>;
    update(id: string, dto: Partial<PathDto>): Promise<number>;
    delete(id: any): Promise<number>;
    private readById;
}

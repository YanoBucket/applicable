"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const path_entity_1 = require("../../../entity/calcul/path.entity");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const path_model_1 = require("../../../model/calcul/path.model");
const group_model_1 = require("../../../model/auth/group.model");
let PathService = class PathService {
    constructor(pathRepository) {
        this.pathRepository = pathRepository;
        this.NOT_FOUND = "Chemin introuvable";
        this.COMP_TAKEN = "Deux compartiment avec la même relation existent déjà pour ce groupe";
        this.logger = new common_1.Logger("PathService");
    }
    async findAll() {
        return await this.pathRepository.find({
            relations: ['GRP', 'GRP.TYP']
        }).then(items => items.map(e => path_model_1.PathDto.fromEntity(e)));
    }
    async findAllByGroup(dto) {
        return this.pathRepository.find({
            relations: ['GRP', 'GRP.TYP'],
            where: { GRP_Id: dto.id }
        }).then(items => items.map(e => path_model_1.PathDto.fromEntity(e)));
    }
    async create(dto) {
        try {
            const path = await this.pathRepository.save(dto.toEntity());
            return await this.readById(path.PAT_Id);
        }
        catch (err) {
            this.logger.log(err.errno);
            if (err.errno == '1062') {
                throw new common_1.ConflictException(this.COMP_TAKEN);
            }
            throw err;
        }
    }
    async update(id, dto) {
        try {
            const path = await this.readById(id);
            const stmt = await this.pathRepository.update({ PAT_Id: path.id }, dto.toEntity());
            return stmt.raw.changedRows;
        }
        catch (err) {
            this.logger.log(err.errno);
            if (err.errno == '1062') {
                throw new common_1.ConflictException(this.COMP_TAKEN);
            }
            throw err;
        }
    }
    async delete(id) {
        const path = await this.readById(id);
        const stmt = await this.pathRepository.delete({ PAT_Id: path.id });
        return stmt.raw.affectedRows;
    }
    async readById(id) {
        const path = await this.pathRepository
            .findOne({
            relations: ["GRP"],
            where: { PAT_Id: id }
        });
        if (!path) {
            throw new common_1.HttpException(this.NOT_FOUND, common_1.HttpStatus.NOT_FOUND);
        }
        return path_model_1.PathDto.fromEntity(path);
    }
};
PathService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(path_entity_1.Path)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], PathService);
exports.PathService = PathService;
//# sourceMappingURL=path.service.js.map
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const setting_service_1 = require("./setting.service");
const setting_model_ts_1 = require("../../../model/calcul/setting.model.ts");
const role_guard_1 = require("../../../common/security/role.guard");
const passport_1 = require("@nestjs/passport");
const role_decorator_1 = require("../../../common/decorators/role.decorator");
let SettingController = class SettingController {
    constructor(settingService) {
        this.settingService = settingService;
        this.logger = new common_1.Logger("SettingController");
    }
    findAllPath() {
        return this.settingService.findAll();
    }
    create(data) {
        this.logger.log(`Nouveau setting : ${JSON.stringify(data)}`);
        return this.settingService.create(new setting_model_ts_1.SettingDto(data));
    }
    update(id, data) {
        this.logger.log(`Mise à jour : ${JSON.stringify(data)} sur le setting d'identifiant : ${id}`);
        return this.settingService.update(id, new setting_model_ts_1.SettingDto(data));
    }
    delete(id) {
        return this.settingService.delete(id);
    }
};
__decorate([
    common_1.Get(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], SettingController.prototype, "findAllPath", null);
__decorate([
    common_1.Post(),
    common_1.UseGuards(passport_1.AuthGuard(), role_guard_1.RolesGuard),
    role_decorator_1.RoleDecorator('administrateur'),
    common_1.UsePipes(new common_1.ValidationPipe()),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [setting_model_ts_1.SettingDto]),
    __metadata("design:returntype", Promise)
], SettingController.prototype, "create", null);
__decorate([
    common_1.Put(':id'),
    common_1.UseGuards(passport_1.AuthGuard(), role_guard_1.RolesGuard),
    role_decorator_1.RoleDecorator('administrateur'),
    common_1.UsePipes(new common_1.ValidationPipe()),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], SettingController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    common_1.UseGuards(passport_1.AuthGuard(), role_guard_1.RolesGuard),
    role_decorator_1.RoleDecorator('administrateur'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], SettingController.prototype, "delete", null);
SettingController = __decorate([
    common_1.Controller('setting'),
    __metadata("design:paramtypes", [setting_service_1.SettingService])
], SettingController);
exports.SettingController = SettingController;
//# sourceMappingURL=setting.controller.js.map
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const setting_model_ts_1 = require("../../../model/calcul/setting.model.ts");
const typeorm_1 = require("@nestjs/typeorm");
const setting_entity_1 = require("../../../entity/calcul/setting.entity");
const typeorm_2 = require("typeorm");
let SettingService = class SettingService {
    constructor(settingRepository) {
        this.settingRepository = settingRepository;
        this.NOT_FOUND = "Paramètre introuvable";
        this.KEY_TAKEN = "Une clé avec cette valeur existe déjà pour ce groupe";
        this.logger = new common_1.Logger("SettingService");
    }
    async findAll() {
        return await this.settingRepository.find({
            relations: ['GRP', 'GRP.TYP']
        }).then(items => items.map(e => setting_model_ts_1.SettingDto.fromEntity(e)));
    }
    async create(dto) {
        try {
            const setting = await this.settingRepository.save(dto.toEntity());
            return await this.readById(setting.SET_Id);
        }
        catch (err) {
            if (err.errno == '1062') {
                throw new common_1.ConflictException(this.KEY_TAKEN);
            }
            throw err;
        }
    }
    async update(id, dto) {
        try {
            const setting = await this.readById(id);
            const stmt = await this.settingRepository.update({ SET_Id: setting.id }, dto.toEntity());
            return stmt.raw.changedRows;
        }
        catch (err) {
            this.logger.log(err.errno);
            if (err.errno == '1062') {
                throw new common_1.ConflictException(this.KEY_TAKEN);
            }
            throw err;
        }
    }
    async delete(id) {
        const setting = await this.readById(id);
        const stmt = await this.settingRepository.delete({ SET_Id: setting.id });
        return stmt.raw.affectedRows;
    }
    async readById(id) {
        const setting = await this.settingRepository
            .findOne({
            relations: ["GRP"],
            where: { SET_Id: id }
        });
        if (!setting) {
            throw new common_1.HttpException(this.NOT_FOUND, common_1.HttpStatus.NOT_FOUND);
        }
        this.logger.log(setting_model_ts_1.SettingDto.fromEntity(setting));
        return setting_model_ts_1.SettingDto.fromEntity(setting);
    }
};
SettingService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(setting_entity_1.Setting)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], SettingService);
exports.SettingService = SettingService;
//# sourceMappingURL=setting.service.js.map
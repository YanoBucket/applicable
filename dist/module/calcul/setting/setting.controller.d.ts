import { SettingService } from './setting.service';
import { SettingDto } from 'src/model/calcul/setting.model.ts';
export declare class SettingController {
    private settingService;
    private logger;
    constructor(settingService: SettingService);
    findAllPath(): Promise<SettingDto[]>;
    create(data: SettingDto): Promise<SettingDto>;
    update(id: string, data: Partial<SettingDto>): Promise<number>;
    delete(id: string): Promise<number>;
}

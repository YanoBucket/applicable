import { SettingDto } from 'src/model/calcul/setting.model.ts';
import { Setting } from 'src/entity/calcul/setting.entity';
import { Repository } from 'typeorm';
export declare class SettingService {
    private settingRepository;
    private NOT_FOUND;
    private KEY_TAKEN;
    private logger;
    constructor(settingRepository: Repository<Setting>);
    findAll(): Promise<SettingDto[]>;
    create(dto: SettingDto): Promise<SettingDto>;
    update(id: string, dto: Partial<SettingDto>): Promise<number>;
    delete(id: any): Promise<number>;
    private readById;
}

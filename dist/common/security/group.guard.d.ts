import { CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
export declare class groupGuard implements CanActivate {
    private reflector;
    constructor(reflector: Reflector);
    private logger;
    canActivate(context: ExecutionContext): boolean;
    private matchRoles;
}

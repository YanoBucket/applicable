import { User } from 'src/entity/auth/user.entity';
import { Repository } from 'typeorm';
import { UserService } from 'src/module/user/users.service';
declare const JwtStrategy_base: new (...args: any[]) => any;
export declare class JwtStrategy extends JwtStrategy_base {
    private userRepository;
    private userService;
    private logger;
    constructor(userRepository: Repository<User>, userService: UserService);
    validate(payload: any): Promise<import("../../model/auth/user.model").UserDto>;
}
export {};

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const user_model_1 = require("../../model/auth/user.model");
const group_entity_1 = require("../../entity/auth/group.entity");
let groupGuard = class groupGuard {
    constructor(reflector) {
        this.reflector = reflector;
        this.logger = new common_1.Logger("GroupGuard");
    }
    canActivate(context) {
        const groups = this.reflector.get('groups', context.getHandler());
        if (!groups) {
            return true;
        }
        const request = context.switchToHttp().getRequest();
        const user = request.user;
        this.logger.log(user.groups);
        this.logger.log(groups);
        return this.matchRoles(groups, user.role.name);
    }
    matchRoles(needed, attempt) {
        return needed.indexOf(attempt) > -1;
    }
};
groupGuard = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [core_1.Reflector])
], groupGuard);
exports.groupGuard = groupGuard;
//# sourceMappingURL=group.guard.js.map
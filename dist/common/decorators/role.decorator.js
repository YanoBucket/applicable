"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const logger = new common_1.Logger("RoleDecorator");
exports.RoleDecorator = (...role) => common_1.SetMetadata('role', role);
//# sourceMappingURL=role.decorator.js.map
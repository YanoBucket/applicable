"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
let HttpErrorFilter = class HttpErrorFilter {
    constructor() {
        this.logger = new common_1.Logger("HttpFilter");
    }
    catch(exception, host) {
        const context = host.switchToHttp();
        const request = context.getRequest();
        const response = context.getResponse();
        const status = (exception instanceof common_1.HttpException) ? exception.getStatus() : common_1.HttpStatus.INTERNAL_SERVER_ERROR;
        const errorResponse = {
            code: status,
            timestamp: new Date().toLocaleString(),
            path: request.url,
            method: request.method,
            message: typeof exception.getResponse === 'function' ? exception.getResponse() || exception.getResponse()['message'] : exception.message
        };
        common_1.Logger.error(`${request.method} ${request.url}`, JSON.stringify(errorResponse), 'HttpErrorFilter');
        response.status(404).json(errorResponse);
    }
};
HttpErrorFilter = __decorate([
    common_1.Catch()
], HttpErrorFilter);
exports.HttpErrorFilter = HttpErrorFilter;
//# sourceMappingURL=http-error.filter.js.map
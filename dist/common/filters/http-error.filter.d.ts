import { ExceptionFilter, HttpException, ArgumentsHost } from '@nestjs/common';
export declare class HttpErrorFilter implements ExceptionFilter {
    private logger;
    catch(exception: HttpException, host: ArgumentsHost): void;
}

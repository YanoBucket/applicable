-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  192.168.0.54:3306
-- Généré le :  Mer 10 Juin 2020 à 09:31
-- Version du serveur :  5.7.30-0ubuntu0.18.04.1
-- Version de PHP :  7.2.24-0ubuntu0.18.04.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `applicable`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_cable`
--

CREATE TABLE `t_cable` (
  `CAB_Id` varchar(36) NOT NULL,
  `CAB_Length` double NOT NULL,
  `CAB_Quantity` int(11) NOT NULL,
  `CON_Id_A` varchar(255) NOT NULL,
  `CON_Id_B` varchar(255) NOT NULL,
  `CAT_Id` varchar(255) NOT NULL,
  `GRP_Id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_cable_category`
--

CREATE TABLE `t_cable_category` (
  `CAB_CAT_Id` varchar(36) NOT NULL,
  `CAB_CAT_Name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_connector`
--

CREATE TABLE `t_connector` (
  `CON_Id` varchar(36) NOT NULL,
  `CON_Name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_group`
--

CREATE TABLE `t_group` (
  `GRP_Id` varchar(36) NOT NULL,
  `GRP_Name` varchar(25) NOT NULL,
  `TYP_Id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_group_type`
--

CREATE TABLE `t_group_type` (
  `GRP_TYP_Id` varchar(36) NOT NULL,
  `GRP_TYP_Name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_path`
--

CREATE TABLE `t_path` (
  `PAT_Id` varchar(36) NOT NULL,
  `PAT_Distance` double NOT NULL,
  `PAT_Compartment_A` char(8) NOT NULL,
  `PAT_Compartment_B` char(8) NOT NULL,
  `GRP_Id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_role`
--

CREATE TABLE `t_role` (
  `ROL_Id` varchar(36) NOT NULL,
  `ROL_Name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_setting`
--

CREATE TABLE `t_setting` (
  `SET_Id` varchar(36) NOT NULL,
  `SET_Key` varchar(20) NOT NULL,
  `SET_Value` float NOT NULL,
  `GRP_Id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_sfp`
--

CREATE TABLE `t_sfp` (
  `SFP_Id` varchar(36) NOT NULL,
  `SFP_speed` varchar(20) NOT NULL,
  `SFP_quantity` int(11) NOT NULL,
  `SFP_Comment` varchar(100) DEFAULT NULL,
  `SFP_Utilisation` varchar(50) NOT NULL,
  `CAT_Id` varchar(255) NOT NULL,
  `GRP_Id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_sfp_category`
--

CREATE TABLE `t_sfp_category` (
  `SFP_CAT_Id` varchar(36) NOT NULL,
  `SFP_CAT_Name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_user`
--

CREATE TABLE `t_user` (
  `USE_Id` varchar(36) NOT NULL,
  `USE_Username` varchar(20) NOT NULL,
  `USE_Password` varchar(255) NOT NULL,
  `USE_Role` varchar(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_user_groups`
--

CREATE TABLE `t_user_groups` (
  `USE_Id` varchar(36) NOT NULL,
  `GRP_Id` varchar(36) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_cable`
--
ALTER TABLE `t_cable`
  ADD PRIMARY KEY (`CAB_Id`),
  ADD KEY `FK_55cfb6f759635b3e4ff545af5bd` (`CON_Id_A`),
  ADD KEY `FK_fc2c84a991e4200932a1b1d75f5` (`CON_Id_B`),
  ADD KEY `FK_39716899d52e6b022aca500a540` (`CAT_Id`),
  ADD KEY `FK_47de4fe27d131a809ed4712a830` (`GRP_Id`);

--
-- Index pour la table `t_cable_category`
--
ALTER TABLE `t_cable_category`
  ADD PRIMARY KEY (`CAB_CAT_Id`),
  ADD UNIQUE KEY `IDX_3c16a14fdfb905ab8dbbd12366` (`CAB_CAT_Name`);

--
-- Index pour la table `t_connector`
--
ALTER TABLE `t_connector`
  ADD PRIMARY KEY (`CON_Id`),
  ADD UNIQUE KEY `IDX_729ffe5f58f3e79d32258dd09f` (`CON_Name`);

--
-- Index pour la table `t_group`
--
ALTER TABLE `t_group`
  ADD PRIMARY KEY (`GRP_Id`),
  ADD UNIQUE KEY `IDX_ff29ab0256bce84e74c1c7562e` (`GRP_Name`,`TYP_Id`),
  ADD KEY `FK_02b113f05e73824d19e7e77e28f` (`TYP_Id`);

--
-- Index pour la table `t_group_type`
--
ALTER TABLE `t_group_type`
  ADD PRIMARY KEY (`GRP_TYP_Id`);

--
-- Index pour la table `t_path`
--
ALTER TABLE `t_path`
  ADD PRIMARY KEY (`PAT_Id`),
  ADD UNIQUE KEY `IDX_Comp` (`PAT_Compartment_A`,`PAT_Compartment_B`,`GRP_Id`),
  ADD KEY `FK_26394a4ea1523a98b0040dfad4e` (`GRP_Id`);

--
-- Index pour la table `t_role`
--
ALTER TABLE `t_role`
  ADD PRIMARY KEY (`ROL_Id`),
  ADD UNIQUE KEY `IDX_06487bab727f1935b493d18810` (`ROL_Name`);

--
-- Index pour la table `t_setting`
--
ALTER TABLE `t_setting`
  ADD PRIMARY KEY (`SET_Id`),
  ADD UNIQUE KEY `IDX_Key` (`SET_Key`,`GRP_Id`),
  ADD KEY `FK_e79fd3e3588d5dff5ec17b6aac3` (`GRP_Id`);

--
-- Index pour la table `t_sfp`
--
ALTER TABLE `t_sfp`
  ADD PRIMARY KEY (`SFP_Id`),
  ADD KEY `FK_c0b4fbd0adb3e874fe6c2c35311` (`CAT_Id`),
  ADD KEY `FK_820013246b670de921db062c654` (`GRP_Id`);

--
-- Index pour la table `t_sfp_category`
--
ALTER TABLE `t_sfp_category`
  ADD PRIMARY KEY (`SFP_CAT_Id`),
  ADD UNIQUE KEY `IDX_e19f81c66627a112d3acdf48c4` (`SFP_CAT_Name`);

--
-- Index pour la table `t_user`
--
ALTER TABLE `t_user`
  ADD PRIMARY KEY (`USE_Id`),
  ADD UNIQUE KEY `IDX_421d9821b9d2a779b929f2a54d` (`USE_Username`),
  ADD KEY `FK_d0c851b8e9b60f4e3583768ed97` (`USE_Role`);

--
-- Index pour la table `t_user_groups`
--
ALTER TABLE `t_user_groups`
  ADD PRIMARY KEY (`USE_Id`,`GRP_Id`),
  ADD KEY `IDX_ed8bbed2d1ad4c78f47ad61c32` (`USE_Id`),
  ADD KEY `IDX_0609bff3bca9343f05401745aa` (`GRP_Id`);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `t_cable`
--
ALTER TABLE `t_cable`
  ADD CONSTRAINT `FK_39716899d52e6b022aca500a540` FOREIGN KEY (`CAT_Id`) REFERENCES `t_cable_category` (`CAB_CAT_Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_47de4fe27d131a809ed4712a830` FOREIGN KEY (`GRP_Id`) REFERENCES `t_group` (`GRP_Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_55cfb6f759635b3e4ff545af5bd` FOREIGN KEY (`CON_Id_A`) REFERENCES `t_connector` (`CON_Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_fc2c84a991e4200932a1b1d75f5` FOREIGN KEY (`CON_Id_B`) REFERENCES `t_connector` (`CON_Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `t_group`
--
ALTER TABLE `t_group`
  ADD CONSTRAINT `FK_02b113f05e73824d19e7e77e28f` FOREIGN KEY (`TYP_Id`) REFERENCES `t_group_type` (`GRP_TYP_Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `t_path`
--
ALTER TABLE `t_path`
  ADD CONSTRAINT `FK_26394a4ea1523a98b0040dfad4e` FOREIGN KEY (`GRP_Id`) REFERENCES `t_group` (`GRP_Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `t_setting`
--
ALTER TABLE `t_setting`
  ADD CONSTRAINT `FK_e79fd3e3588d5dff5ec17b6aac3` FOREIGN KEY (`GRP_Id`) REFERENCES `t_group` (`GRP_Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `t_sfp`
--
ALTER TABLE `t_sfp`
  ADD CONSTRAINT `FK_820013246b670de921db062c654` FOREIGN KEY (`GRP_Id`) REFERENCES `t_group` (`GRP_Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_c0b4fbd0adb3e874fe6c2c35311` FOREIGN KEY (`CAT_Id`) REFERENCES `t_sfp_category` (`SFP_CAT_Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `t_user`
--
ALTER TABLE `t_user`
  ADD CONSTRAINT `FK_d0c851b8e9b60f4e3583768ed97` FOREIGN KEY (`USE_Role`) REFERENCES `t_role` (`ROL_Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `t_user_groups`
--
ALTER TABLE `t_user_groups`
  ADD CONSTRAINT `FK_0609bff3bca9343f05401745aa4` FOREIGN KEY (`GRP_Id`) REFERENCES `t_group` (`GRP_Id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_ed8bbed2d1ad4c78f47ad61c32c` FOREIGN KEY (`USE_Id`) REFERENCES `t_user` (`USE_Id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
